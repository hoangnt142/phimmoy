#!/bin/sh

if [ -z "$RAILS_ENV" ]; then
    rails_env=development
else
    rails_env=$RAILS_ENV
fi

# Remove zombie process
echo Deleting old process...
[ -f tmp/pids/server.pid ] && rm tmp/pids/server.pid


mkdir -p ~/.ssh
cat key | tr -d '\r' > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
ssh-keygen -y -f ~/.ssh/id_rsa > ~/.ssh/id_rsa.pub
ssh-keyscan -H 45.117.82.79 >> ~/.ssh/known_hosts

bundle check || bundle install

rake assets:precompile 

rake db:migrate

# update crontab
whenever --update-crontab --set "environment=${rails_env}&cron_log=/tmp/line_remind.log"

# ensure cron service 's running
(service cron status | grep -i 'is running') || service cron restart

# Start Rails server
echo Starting server...
bundle exec rails s -b 0.0.0.0 -e $rails_env