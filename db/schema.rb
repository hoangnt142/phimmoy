# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_31_180205) do

  create_table "black_lists", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "film_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "url"
    t.index ["film_id"], name: "index_black_lists_on_film_id"
  end

  create_table "categories", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "codes", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "customer_id"
    t.integer "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code_string"
    t.index ["customer_id"], name: "index_codes_on_customer_id"
  end

  create_table "customers", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "phone_number"
    t.integer "marked", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "episodes", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "label"
    t.string "url", limit: 10000
    t.bigint "server_id"
    t.integer "vtv16_id"
    t.text "medias"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "source"
    t.integer "source_id"
    t.integer "facebook_token_id"
    t.string "video_id"
    t.string "video_url", limit: 1000
    t.integer "upload_success_part", default: 0
    t.string "upload_session_id"
    t.boolean "upload_finished", default: false
    t.string "start_offset"
    t.boolean "downloaded", default: false
    t.integer "upload_server"
    t.integer "video_size"
    t.index ["server_id"], name: "index_episodes_on_server_id"
  end

  create_table "facebook_tokens", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "fb_page_id"
    t.string "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "page_name"
  end

  create_table "film_categories", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "film_id"
    t.bigint "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_id"], name: "index_film_categories_on_category_id"
    t.index ["film_id"], name: "index_film_categories_on_film_id"
  end

  create_table "film_nationalities", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "film_id"
    t.bigint "nationality_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["film_id"], name: "index_film_nationalities_on_film_id"
    t.index ["nationality_id"], name: "index_film_nationalities_on_nationality_id"
  end

  create_table "film_starings", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "film_id"
    t.bigint "staring_id"
    t.string "name"
    t.boolean "is_director", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["film_id"], name: "index_film_starings_on_film_id"
    t.index ["staring_id"], name: "index_film_starings_on_staring_id"
  end

  create_table "films", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "film_type", default: 1, null: false
    t.integer "source_id"
    t.integer "source"
    t.string "title"
    t.string "eng_title"
    t.string "trailer_url", limit: 1000
    t.text "content"
    t.bigint "movie_type_id"
    t.string "full_path"
    t.integer "number", default: 1
    t.text "seo_description"
    t.text "og_description"
    t.string "seo_title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "year_id"
    t.date "publish_date"
    t.float "imdb_rate"
    t.string "avatar_url"
    t.string "length"
    t.boolean "marked", default: false
    t.string "url"
    t.string "cover_image_file_name"
    t.string "cover_image_content_type"
    t.integer "cover_image_file_size"
    t.datetime "cover_image_updated_at"
    t.string "poster_image_file_name"
    t.string "poster_image_content_type"
    t.integer "poster_image_file_size"
    t.datetime "poster_image_updated_at"
    t.string "detail_url"
    t.string "poster_url"
    t.string "cover_url"
    t.text "posters"
    t.text "covers"
    t.string "bilu_get_link_player"
    t.boolean "filled_info", default: true
    t.index ["eng_title"], name: "fi_films_2", type: :fulltext
    t.index ["movie_type_id"], name: "index_films_on_movie_type_id"
    t.index ["title"], name: "fi_films_1", type: :fulltext
    t.index ["url"], name: "ui_films_3", unique: true
    t.index ["year_id"], name: "index_films_on_year_id"
  end

  create_table "medias", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "episode_id"
    t.text "response"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["episode_id"], name: "index_medias_on_episode_id"
  end

  create_table "movie_types", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "nationalities", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "page_views", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "film_id"
    t.integer "view_count", default: 1
    t.date "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["film_id"], name: "index_page_views_on_film_id"
  end

  create_table "pages", id: :integer, unsigned: true, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "pageable_id", unsigned: true
    t.string "pageable_type"
    t.string "title"
    t.string "url", null: false
    t.string "seo_title"
    t.text "seo_description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "og_description"
    t.integer "publish_status", default: 1
    t.index ["url"], name: "ui_pages_1", unique: true
  end

  create_table "proxies", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "address"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "servers", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.bigint "film_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["film_id"], name: "index_servers_on_film_id"
  end

  create_table "starings", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "name2"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "system_configs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "proxy", default: "http://185.93.3.123:8080"
    t.datetime "proxy_updated_at", default: "2018-09-30 17:40:08"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "cookies"
  end

  create_table "years", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "codes", "customers"
  add_foreign_key "episodes", "servers"
  add_foreign_key "film_categories", "categories"
  add_foreign_key "film_categories", "films"
  add_foreign_key "film_nationalities", "films"
  add_foreign_key "film_nationalities", "nationalities"
  add_foreign_key "film_starings", "films"
  add_foreign_key "film_starings", "starings"
  add_foreign_key "films", "movie_types"
  add_foreign_key "films", "years"
  add_foreign_key "medias", "episodes"
  add_foreign_key "page_views", "films"
  add_foreign_key "servers", "films"
end
