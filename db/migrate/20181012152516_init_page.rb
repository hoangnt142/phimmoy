class InitPage < ActiveRecord::Migration[5.2]
  def change
    count = Film.count
    Film.all.each_with_index do |film, i|
      puts "INIT film #{i}/#{count}"
      page = film.build_page
      page.title = film.title
      page.seo_title = film.seo_title
      page.seo_description = film.seo_description
      page.url = "/#{film.url}"
      page.save
    end
    Category.all.each do |cat|
      page = cat.build_page
      page.title = "Phim #{cat.name.downcase} hay | Phim #{cat.name.downcase} mới | Phim #{cat.name.downcase} Mỹ"
      page.seo_title = "Phim #{cat.name.downcase} hay | Phim #{cat.name.downcase} mới | Phim #{cat.name.downcase} Mỹ"
      page.seo_description = "Phim #{cat.name.downcase} hay | Phim #{cat.name.downcase} mới | Phim #{cat.name.downcase} Mỹ"
      page.url = "/the-loai/#{cat.slug}"
      page.save
    end
    Year.all.each do |year|
      if year.year > 2011
        page1 = year.build_page(
          url: "/phim-le/#{year.year}",
          title: "Phim lẻ #{year.year} | Phim lẻ #{year.year} hay tuyển chọn | Phim lẻ #{year.year} mới nhất",
          seo_title: "Phim lẻ #{year.year} | Phim lẻ #{year.year} hay tuyển chọn | Phim lẻ #{year.year} mới nhất",
          seo_description: "Phim lẻ #{year.year} | Phim lẻ #{year.year} hay tuyển chọn | Phim lẻ #{year.year} mới nhất",
        )
        page1.save
      end
    end

    Page.create(
      url: '/',
      title: 'MitdacTV - Xem Phim Nhanh, Xem Phim Online, Phim VietSub, Thuyết Minh Hay Nhất',
      seo_title: 'MitdacTV - Xem Phim Nhanh, Xem Phim Online, Phim VietSub, Thuyết Minh Hay Nhất',
      seo_description: 'Xem phim mới miễn phí nhanh chất lượng cao. Xem Phim online Việt Sub, Thuyết minh, lồng tiếng chất lượng HD. Xem phim nhanh online chất lượng cao'
    )

    Page.create(
      url: '/phim-le',
      title: 'Phim lẻ  | Phim lẻ  hay tuyển chọn | Phim lẻ  mới nhất  2018',
      seo_title: 'Phim lẻ  | Phim lẻ  hay tuyển chọn | Phim lẻ  mới nhất  2018',
      seo_description: 'Phim lẻ, Phim lẻ  | Phim lẻ  hay tuyển chọn | Phim lẻ  mới nhất  2018'
    )

    Page.create(
      url: '/phim-bo',
      title: 'Phim bộ  | Phim bộ  hay tuyển chọn | Phim bộ  mới nhất  2018',
      seo_title: 'Phim bộ  | Phim bộ  hay tuyển chọn | Phim bộ  mới nhất  2018',
      seo_description: 'Phim bộ  | Phim bộ  hay tuyển chọn | Phim bộ  mới nhất  2018'
    )

    Nationality.where(id: [1, 2, 3, 4, 5, 6, 7, 8, 9, 19, 30]).each do |nat|
      page1 = nat.build_page(
        url: "/quoc-gia/#{nat.slug}",
        title: "Phim #{nat.name} hay tuyển chọn | Phim #{nat.name} mới nhất 2018",
        seo_title: "Phim #{nat.name} hay tuyển chọn | Phim #{nat.name} mới nhất 2018",
        seo_description: "Phim Trung Quốc, Phim #{nat.name} hay tuyển chọn | Phim #{nat.name} mới nhất 2018"
      )
      page2 = nat.build_page(
        url: "/phim-bo/#{nat.slug}",
        title: "Phim bộ #{nat.name} | Phim bộ #{nat.name} hay tuyển chọn | Phim bộ #{nat.name} mới nhất  2018",
        seo_title: "Phim bộ #{nat.name} | Phim bộ #{nat.name} hay tuyển chọn | Phim bộ #{nat.name} mới nhất  2018",
        seo_description: "Phim bộ - Phim #{nat.name}, Phim bộ #{nat.name}  | Phim bộ #{nat.name}  hay tuyển chọn | Phim bộ #{nat.name}  mới nhất  2018",
      )
      page1.save
      page2.save
    end

  end
end
