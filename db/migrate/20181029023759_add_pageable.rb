class AddPageable < ActiveRecord::Migration[5.2]
  def change
    Page.where(pageable_type: nil).update_all(pageable_type: 'ListFilm')
    Page.find_by(url: "/").update(pageable_type: 'Index', url: '')
    Page.where(pageable_type: "Year").update_all(pageable_type: "ListFilm")
    Page.where(pageable_type: "Category").destroy_all
    Category.all.each do |cat|
      Page.find_by(url: "/the-loai/#{cat.slug}").try(:destroy)
      cat.create_page(
        title: cat.name,
        url: "/the-loai/#{cat.slug}"
      )
    end
    Page.create(pageable_type: 'ListFilm', url: '/phim-le/truoc-2012')
    Page.create(
      pageable_type: 'ListFilm',
      url: '/phim-le',
      title: 'Phim lẻ  | Phim lẻ  hay tuyển chọn | Phim lẻ  mới nhất  2018',
      seo_title: 'Phim lẻ  | Phim lẻ  hay tuyển chọn | Phim lẻ  mới nhất  2018',
      seo_description: 'Phim lẻ, Phim lẻ  | Phim lẻ  hay tuyển chọn | Phim lẻ  mới nhất  2018'
    )

    Page.create(
      pageable_type: 'ListFilm',
      url: '/phim-bo',
      title: 'Phim bộ  | Phim bộ  hay tuyển chọn | Phim bộ  mới nhất  2018',
      seo_title: 'Phim bộ  | Phim bộ  hay tuyển chọn | Phim bộ  mới nhất  2018',
      seo_description: 'Phim bộ  | Phim bộ  hay tuyển chọn | Phim bộ  mới nhất  2018'
    )
  end
end
