class ChangeColumnMovieUrl < ActiveRecord::Migration[5.2]
  def change
    change_column :pages, :movie_url, :string, :limit => 10000
  end
end
