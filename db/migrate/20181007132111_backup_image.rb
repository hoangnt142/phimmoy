class BackupImage < ActiveRecord::Migration[5.2]
  def change
    Film.bilutv.all.find_each do |film|
      if film.poster_image
        puts "Restoring #{film.id}"
        film.update(
          posters: (File.open(film.poster_image.path) rescue nil),
          covers: (File.open(film.cover_image.path) rescue nil)
        )
      end
    end
  end
end
