class AddResponseToEpisodes < ActiveRecord::Migration[5.2]
  def change
    add_column :episodes, :response, :text, after: :crawl_url
  end
end
