class InitData < ActiveRecord::Migration[5.2]
  def change
    MovieType.create(name: 'Phim Lẻ')
    MovieType.create(name: 'Phim Bộ')
  end
end
