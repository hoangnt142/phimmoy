class RemoveColumFromFilm < ActiveRecord::Migration[5.2]
  def change
    remove_column :films, :category_id
    remove_column :films, :nationality_id
  end
end
