class AddVtv16CrawlUrlToEpisodes < ActiveRecord::Migration[5.2]
  def change
    add_column :episodes, :vtv16_crawl_url, :string, after: :crawl_url
  end
end
