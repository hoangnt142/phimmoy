class CreatePages < ActiveRecord::Migration[5.2]
  def change
    create_table :pages, id: :integer, unsigned: true, force: true do |t|
      t.integer :pageable_id, unsigned: true, default: nil
      t.string :pageable_type, default: nil
      t.string :title
      t.string :url, null: false
      t.string :seo_title, default: nil
      t.text :seo_description, default: nil
      t.timestamps
    end

    execute "CREATE UNIQUE INDEX ui_pages_1 ON pages(url)"
  end
end
