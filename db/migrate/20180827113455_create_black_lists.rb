class CreateBlackLists < ActiveRecord::Migration[5.2]
  def change
    create_table :black_lists do |t|
      t.integer :film_id, index: true
      t.timestamps
    end
  end
end
