class ChangeColumnPage < ActiveRecord::Migration[5.2]
  def change
    change_column :pages, :og_description, :text
  end
end
