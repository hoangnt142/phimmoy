class CreateCodes < ActiveRecord::Migration[5.2]
  def change
    create_table :codes, force: true do |t|
      t.references :customer, foreign_key: true
      t.integer :code

      t.timestamps
    end
  end
end
