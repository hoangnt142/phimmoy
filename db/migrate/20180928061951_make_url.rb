class MakeUrl < ActiveRecord::Migration[5.2]
  def change

    Film.all.find_each do |film|
      title = film.title
      url = Page.name_slug(title)
      puts "---#{title}---#{url}"
      begin
        film.update(url: url)
      rescue
        # film.destroy
      end
    end

  end
end
