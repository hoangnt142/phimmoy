class CreateProxies < ActiveRecord::Migration[5.2]
  def change
    create_table :proxies do |t|
      t.string :address

      t.timestamps
    end
    [
      'http://mysmaily:105894@p1.vietpn.co:1808',
      'http://mysmaily:105894@p2.vietpn.co:1808',
      'http://mysmaily:105894@p4.vietpn.co:1808',
      'http://mysmaily:105894@p5.vietpn.co:1808',
      'http://mysmaily:105894@p6.vietpn.co:1808',
      'http://mysmaily:105894@p7.vietpn.co:1808',
      'http://mysmaily:105894@p10.vietpn.co:1808',
      'http://mysmaily:105894@p12.vietpn.co:1808',
      'http://mysmaily:105894@p13.vietpn.co:1808',
      'http://mysmaily:105894@p14.vietpn.co:1808',
      'http://mysmaily:105894@p15.vietpn.co:1808',
      'http://mysmaily:105894@s.vietpn.co:1808',
      'http://mysmaily:105894@s2.vietpn.co:1808',
      'http://mysmaily:105894@s4.vietpn.co:1808',
      'http://mysmaily:105894@s11.vietpn.co:1808',
      'http://mysmaily:105894@222.255.179.164:1808',
      'http://mysmaily:105894@s9.vietpn.co:1808',
      'http://mysmaily:105894@42.112.30.20:3130',
      'http://mysmaily:105894@42.112.30.23:3129',
      'http://mysmaily:105894@42.112.30.30:3130',
      'http://mysmaily:105894@42.112.30.66:3131'
    ].each do |link|
      Proxy.create(address: link)
    end
  end
end
