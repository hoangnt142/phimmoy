class AddMediasToEpisodes < ActiveRecord::Migration[5.2]
  def change
    add_column :episodes, :medias, :text, after: :response
  end
end
