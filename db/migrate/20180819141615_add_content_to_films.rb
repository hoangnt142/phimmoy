class AddContentToFilms < ActiveRecord::Migration[5.2]
  def change
    add_column :films, :content, :text, after: :eng_title
  end
end
