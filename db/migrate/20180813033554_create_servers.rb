class CreateServers < ActiveRecord::Migration[5.2]
  def change
    create_table :servers do |t|
      t.string :name
      t.references :film, foreign_key: true, on_delete: :cascade
      t.timestamps
    end
  end
end
