class CreateFacebookTokens < ActiveRecord::Migration[5.2]
  def change
    create_table :facebook_tokens, force: true do |t|
      t.string :fb_page_id
      t.string :token

      t.timestamps
    end

    FacebookToken.create(fb_page_id: 181388382753634, token: "EAAWXzSgMXLIBAO3DVIM13MIIuA9OAZCR6jJ853OnK5ZArd7oawYUa9LX0kZBUyphNL28z6WVrnMnoJuMgOqCzYsPZCFyARfhWiDF3IdeGjOIyRhWkNY49Yi0qdovOCmACjbXyIK32aeYZBZA1UjL3hLO2jwBFzafFYhgOGPmlpniJj4f9MQzYLJb2NhyNKY9D9sUpCsFkqOwZDZD")
  end
end
