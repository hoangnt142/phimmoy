class AddImdbRateToFilms < ActiveRecord::Migration[5.2]
  def change
    add_column :films, :imdb_rate, :float
  end
end
