class CreateFimls < ActiveRecord::Migration[5.2]
  def change
    create_table :films do |t|
      t.string :title
      t.string :eng_title
      t.references :category, foreign_key: true, on_delete: :cascade
      t.string :full_path
      t.integer :number, default: 1
      t.text :seo_description
      t.text :og_description
      t.string :seo_title
      t.timestamps
    end
  end
end
