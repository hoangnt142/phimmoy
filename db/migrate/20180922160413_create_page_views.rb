class CreatePageViews < ActiveRecord::Migration[5.2]
  def change
    create_table :page_views, force: true do |t|
      t.references :film, foreign_key: true
      t.integer :view_count, default: 1
      t.date :date
      t.timestamps
    end
  end
end