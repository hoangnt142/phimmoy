class AddProxyToSystemConfigs < ActiveRecord::Migration[5.2]
  def change
    add_column :system_configs, :proxy_updated_at, :datetime, after: :token, default: Time.now
    add_column :system_configs, :proxy, :string, after: :token, default: "http://185.93.3.123:8080"
  end
end
