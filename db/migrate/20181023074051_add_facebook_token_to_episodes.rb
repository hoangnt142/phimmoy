class AddFacebookTokenToEpisodes < ActiveRecord::Migration[5.2]
  def change
    add_column :episodes, :facebook_token_id, :integer
  end
end
