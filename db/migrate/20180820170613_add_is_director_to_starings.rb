class AddIsDirectorToStarings < ActiveRecord::Migration[5.2]
  def change
    add_column :film_starings, :is_director, :boolean, default: false, after: :name
    add_column :films, :publish_date, :date
    add_reference :films, :nationality, foreign_key: true
  end
end
