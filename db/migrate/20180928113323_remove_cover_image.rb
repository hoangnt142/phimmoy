class RemoveCoverImage < ActiveRecord::Migration[5.2]
  def change
    remove_column :films, :cover_image
    remove_column :films, :poster_image

    add_attachment :films, :cover_image
    add_attachment :films, :poster_image
  end
end
