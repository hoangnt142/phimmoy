class AddSouceAndSouceIdToEpisodes < ActiveRecord::Migration[5.2]
  def change
    add_column :episodes, :source, :integer
    add_column :episodes, :source_id, :integer

    execute "CREATE FULLTEXT INDEX fi_films_1 ON films(title)"
    execute "CREATE FULLTEXT INDEX fi_films_2 ON films(eng_title)"
  end
end
