class AddPosterUrlToFilms < ActiveRecord::Migration[5.2]
  def change
    add_column :films, :poster_image, :string, after: :cover_image
  end
end
