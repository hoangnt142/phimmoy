class AddPosterToFilms < ActiveRecord::Migration[5.2]
  def change
    add_column :films, :posters, :text
  end
end
