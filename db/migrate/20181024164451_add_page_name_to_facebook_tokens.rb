class AddPageNameToFacebookTokens < ActiveRecord::Migration[5.2]
  def change
    add_column :facebook_tokens, :page_name, :string, before: :token
  end
end
