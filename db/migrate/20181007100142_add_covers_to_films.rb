class AddCoversToFilms < ActiveRecord::Migration[5.2]
  def change
    add_column :films, :covers, :text
  end
end
