class CreateFilmStarings < ActiveRecord::Migration[5.2]
  def change
    create_table :film_starings do |t|
      t.references :film, foreign_key: true
      t.references :staring, foreign_key: true
      t.string :name

      t.timestamps
    end
  end
end
