class AddVtv16IdToFilms < ActiveRecord::Migration[5.2]
  def change
    add_column :films, :vtv16_id, :integer, after: :id
  end
end
