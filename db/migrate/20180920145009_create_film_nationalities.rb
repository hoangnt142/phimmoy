class CreateFilmNationalities < ActiveRecord::Migration[5.2]
  def change
    create_table :film_nationalities do |t|
      t.references :film, foreign_key: true
      t.references :nationality, foreign_key: true

      t.timestamps
    end
  end
end
