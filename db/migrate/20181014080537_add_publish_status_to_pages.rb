class AddPublishStatusToPages < ActiveRecord::Migration[5.2]
  def change
    add_column :pages, :publish_status, :integer, default: 1
  end
end
