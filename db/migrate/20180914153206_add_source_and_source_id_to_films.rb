class AddSourceAndSourceIdToFilms < ActiveRecord::Migration[5.2]
  def change
    add_column :films, :source, :integer, after: :id
    add_column :films, :source_id, :integer, index: true, after: :id
    remove_column :films, :vtv16_id
  end
end
