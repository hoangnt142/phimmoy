class AddVideoUrlToEpisodes < ActiveRecord::Migration[5.2]
  def change
    add_column :episodes, :video_url, :string, limit: 1000
  end
end
