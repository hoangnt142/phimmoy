class AddVtv16IdToEpisodes < ActiveRecord::Migration[5.2]
  def change
    add_column :episodes, :vtv16_id, :integer, after: :vtv16_crawl_url
  end
end
