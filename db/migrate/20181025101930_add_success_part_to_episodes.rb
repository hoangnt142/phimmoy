class AddSuccessPartToEpisodes < ActiveRecord::Migration[5.2]
  def change
    add_column :episodes, :upload_success_part, :integer, default: 0
    remove_column :episodes, :vtv16_crawl_url
    add_column :episodes, :upload_session_id, :string
    add_column :episodes, :upload_finished, :boolean, default: false
    remove_column :episodes, :crawl_url
    remove_column :episodes, :response
    add_column :episodes, :start_offset, :string
    add_column :films, :filled_info, :boolean, default: true
  end
end
