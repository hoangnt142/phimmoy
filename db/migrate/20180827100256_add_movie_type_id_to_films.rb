class AddMovieTypeIdToFilms < ActiveRecord::Migration[5.2]
  def change
    add_reference :films, :movie_type, foreign_key: true, after: :category_id
  end
end
