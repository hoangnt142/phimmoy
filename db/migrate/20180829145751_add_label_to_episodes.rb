class AddLabelToEpisodes < ActiveRecord::Migration[5.2]
  def change
    add_column :episodes, :label, :string, after: :id
  end
end
