class AddCodeStringToCodes < ActiveRecord::Migration[5.2]
  def change
    add_column :codes, :code_string, :string
  end
end
