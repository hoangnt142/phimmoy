class CreateEpisodes < ActiveRecord::Migration[5.2]
  def change
    create_table :episodes do |t|
      t.string :url, :limit => 10000
      t.references :server, foreign_key: true, on_delete: :cascade
      t.string :crawl_url, :limit => 10000
      t.timestamps
    end
  end
end
