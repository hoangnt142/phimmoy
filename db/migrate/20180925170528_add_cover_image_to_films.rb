class AddCoverImageToFilms < ActiveRecord::Migration[5.2]
  def change
    add_column :films, :cover_image, :string, after: :avatar_url
    # Film.find(5001).update(cover_image: "https://cdn1.thr.com/sites/default/files/imagecache/scale_crop_768_433/2018/05/__5ada39a9bddb6.jpg")
    # Film.find(6938).update(cover_image: "https://boygeniusreport.files.wordpress.com/2018/05/ant-man-and-the-wasp.jpg?quality=98&strip=all&w=782")
  end
end
