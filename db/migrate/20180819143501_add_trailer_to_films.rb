class AddTrailerToFilms < ActiveRecord::Migration[5.2]
  def change
    add_column :films, :trailer_url, :string, limit: 1000, after: :eng_title
  end
end
