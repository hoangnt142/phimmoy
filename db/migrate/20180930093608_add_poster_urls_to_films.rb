class AddPosterUrlsToFilms < ActiveRecord::Migration[5.2]
  def change
    add_column :films, :poster_url, :string
    add_column :films, :cover_url, :string
  end
end
