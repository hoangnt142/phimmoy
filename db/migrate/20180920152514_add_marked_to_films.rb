class AddMarkedToFilms < ActiveRecord::Migration[5.2]
  def change
    add_column :films, :marked, :boolean, default: false
  end
end
