class CreateSystemConfigs < ActiveRecord::Migration[5.2]
  def change
    create_table :system_configs do |t|
      t.string :token

      t.timestamps
    end
  end
end
