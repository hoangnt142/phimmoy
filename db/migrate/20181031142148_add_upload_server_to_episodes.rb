class AddUploadServerToEpisodes < ActiveRecord::Migration[5.2]
  def change
    add_column :episodes, :upload_server, :integer
  end
end
