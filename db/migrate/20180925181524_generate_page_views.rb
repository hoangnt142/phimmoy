class GeneratePageViews < ActiveRecord::Migration[5.2]
  def change
    PageView.destroy_all
    all = Film.count
    i = 1
    Film.all.find_each do |film|
      puts "ĐAng tạo page view cho phim #{i}/#{all} --- ĐỪNG HỎI"
      film.page_views.create(date: Time.now)
      i+=1
    end
  end
end
