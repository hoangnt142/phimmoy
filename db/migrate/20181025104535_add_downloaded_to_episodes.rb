class AddDownloadedToEpisodes < ActiveRecord::Migration[5.2]
  def change
    add_column :episodes, :downloaded, :boolean, default: false
  end
end
