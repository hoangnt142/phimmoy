class AddVideoSizeToEpisodes < ActiveRecord::Migration[5.2]
  def change
    add_column :episodes, :video_size, :integer
  end
end
