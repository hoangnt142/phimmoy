class AddOgDescriptionToPages < ActiveRecord::Migration[5.2]
  def change
    add_column :pages, :og_description, :string
  end
end
