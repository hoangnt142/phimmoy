class AddDetailUrlToFilms < ActiveRecord::Migration[5.2]
  def change
    add_column :films, :detail_url, :string
  end
end
