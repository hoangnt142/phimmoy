class UpdateMovieType < ActiveRecord::Migration[5.2]
  def change
    Film.update_all(movie_type_id: 1)
  end
end
