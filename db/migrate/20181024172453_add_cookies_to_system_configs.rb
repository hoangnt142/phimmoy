class AddCookiesToSystemConfigs < ActiveRecord::Migration[5.2]
  def change
    add_column :system_configs, :cookies, :string
  end
end
