class AddUrlToBlacklist < ActiveRecord::Migration[5.2]
  def change
    add_column :black_lists, :url, :string
  end
end
