class AddAvatarUrlToFilm < ActiveRecord::Migration[5.2]
  def change
    add_column :films, :avatar_url, :string
  end
end
