class AddUrlToFilms < ActiveRecord::Migration[5.2]
  def change
    add_column :films, :url, :string

    execute "CREATE UNIQUE INDEX ui_films_3 ON films(url)"
  end
end
