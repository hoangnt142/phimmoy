FROM ruby:2.5.1

# FROM ubuntu:16.04

# utility libraries
RUN apt-get update -yq \
    && apt-get install curl gnupg -yq \
    && curl -sL https://deb.nodesource.com/setup_8.x | bash \
    && apt-get install nodejs -yq

RUN apt-get update -qq && apt-get install -y software-properties-common \
 build-essential \
 libpq-dev yarn

# uncomment if use ubuntu image
#RUN apt-add-repository -y ppa:brightbox/ruby-ng
# for a JS runtime, replace by ruby2.5 and ruby2.5-dev if use ubuntu image
RUN apt-get update -qq && apt-get install -y cron ruby-dev

# project files dir
ENV DIR /var/www
RUN mkdir $DIR
WORKDIR $DIR

# bundle setup and install
ADD Gemfile* $DIR/

ENV BUNDLE_GEMFILE=$DIR/Gemfile \
  BUNDLE_JOBS=2

RUN bundle install

RUN gem update --system
RUN bundle binstubs bundler --force

# code pulling
ADD . $DIR