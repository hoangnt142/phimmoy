require 'csv'
namespace :customer do
  task create: :environment do
    Customer.destroy_all
    Code.destroy_all
    CSV.foreach("#{Rails.root}/data/customers.csv").with_index(1) do |row, i|
      customer = Customer.where(name: row[1]).first_or_create(
        name: row.first,
        phone_number: row[1]
      )
      customer.codes.create(
        code_string: row[2]
      )
      puts "---#{i}/#{2052}"
    end

  end
end