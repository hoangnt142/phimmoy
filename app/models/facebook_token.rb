class FacebookToken < ApplicationRecord

  has_many :episodes

  class << self
    def valid(size=100000000, facebook_id=nil)
      if facebook_id
        f = find_by(id: facebook_id)
        return get_f f, size
      end
      where.not(id: 7).order(Arel.sql('rand()')).each do |f|
        begin
          return get_f f, size
        rescue
          next
        end
      end
      return nil
    end

    def get_f(f, size)
      token = f.token
      url = "https://graph-video.facebook.com/v3.2/#{f.fb_page_id}/videos"
      res = RestClient.post(url, {
        access_token: token,
        upload_phase: 'start',
        file_size: size
      })
      if res.headers[:x_page_usage].present?
        x_page_usage = JSON.parse(res.headers[:x_page_usage])
        puts "CHECK #{f.page_name}--#{x_page_usage["total_time"]}"

        if x_page_usage["total_time"] < 90
          return [f, JSON.parse(res)["video_id"], JSON.parse(res)["upload_session_id"]]
        end
      else
        return [f, JSON.parse(res)["video_id"], JSON.parse(res)["upload_session_id"]]
      end
    end

  end

end
