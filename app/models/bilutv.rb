class Bilutv < Film

  default_scope { bilutv }

  class << self

    def bilu_update_info
      i = 1
      bilutv.where(marked: false).find_each do |film|
        url = film.detail_url
        doc = Nokogiri::HTML open(url)

        category_ids = []
        nationality_ids = []

        categories = doc.at("label:contains('Thể loại:')").parent.children.css("a") || []
        nationalities = doc.at("label:contains('Quốc gia:')").parent.children.css("a") || []
        startings = doc.at("label:contains('Diễn viên:')").parent.children.css("a") || []
        b_year = doc.at("label:contains('Năm xuất bản:')").parent.css("span").text rescue nil

        length = doc.at("label:contains('Thời lượng:')").parent.css("span").text rescue nil
        year = Year.where(year: b_year).first_or_create

        categories.each do |cat|
          e_categories = Category.where("name LIKE ? OR name LIKE ?", "%#{cat.text.split("-").last.strip}%", "%#{cat.text.split("-").first.strip}%")
          if e_categories.present?
            category_ids = e_categories.pluck(:id).uniq
          else
            category = Category.where(name: cat.text.strip).first_or_create(slug: "/#{cat[:href]}")
            category_ids << category.id
          end
        end

        
        nationalities.each do |nat|
          national = Nationality.where(name: nat.text.strip).first_or_create(slug: Page.name_slug(nat.text.strip))
          nationality_ids << national.id
        end



        film.update(
          content: doc.css(".film-content").text.strip,
          year_id: year.id,
          nationality_ids: nationality_ids,
          category_ids: category_ids,
          length: length
        )


        directors = doc.at("label:contains('Đạo diễn:')").parent.children.css("a") || []
        directors.each do |dir|

          staring = Staring.where(name: dir.text.strip).first_or_create
          in_film_name = dir.text.strip
          film_staring = film.film_starings.where(staring_id: staring.id, is_director: true).first_or_create(name: in_film_name)
        end



        startings.each do |star|
          staring = Staring.where(name: star.text.strip).first_or_create
          in_film_name = star.text.strip
          film_staring = film.film_starings.where(staring_id: staring.id).first_or_create(name: in_film_name)
        end
        film.update(marked: true)
        i+=1
      end
    end

    def get_images
      bilutv.order(created_at: :DESC).limit(30).each do |film|
        puts film.id
        # begin
          film.update(
            remote_posters_url: (film.poster_url rescue nil),
            remote_covers_url: (film.cover_url rescue nil)
          )
        # rescue

        # end
      end
    end

    def get_image
      bilutv.where(posters: nil).reverse[0..10].each do |film|
        puts film.id
        # begin
          film.update(
            remote_posters_url: film.poster_url,
            remote_covers_url: film_cover_url
          )
        # rescue

        # end
      end
    end

    def aes(m,k,t)
      (aes = OpenSSL::Cipher::Cipher.new('aes-256-cbc').send(m)).key = Digest::SHA256.digest(k)
      aes.update(t) << aes.final
    end

    def encrypt
      key = "bilutv.com45904818776935"
      text = "U2FsdGVkX18qbIfPa4p16zufldyeee5Yow028m2T3iO5VJhA+/HrRRbtwtx1870yVh5Q4TXp7cZwAMDXW3cf2VvXi2eUfdHZdYwWw0ysFzIa4xN0tMBPbt3bo7NnX6qnzdDJ6uq9dsb7uaksEUa2oKW+pbjhQlZjH3GAei7qIYuDPHP3Qdwo4fXg41KnHqfCkeHw5DJLIHWhkw6H2XRBPXsZwCTAUwkS3+8nfrjI8tK+lz+rhZVzksbzbWigkoczB8uj+gsmud9KJ7ebrXEf7QobIML1hEOvto9/muhuQx6p0zjBBr98lrocMPr20fQr"
      
      aes(:encrypt, key, text)
    end

    def decrypt
      key = "bilutv.com45904818776935"
      text = "U2FsdGVkX18qbIfPa4p16zufldyeee5Yow028m2T3iO5VJhA+/HrRRbtwtx1870yVh5Q4TXp7cZwAMDXW3cf2VvXi2eUfdHZdYwWw0ysFzIa4xN0tMBPbt3bo7NnX6qnzdDJ6uq9dsb7uaksEUa2oKW+pbjhQlZjH3GAei7qIYuDPHP3Qdwo4fXg41KnHqfCkeHw5DJLIHWhkw6H2XRBPXsZwCTAUwkS3+8nfrjI8tK+lz+rhZVzksbzbWigkoczB8uj+gsmud9KJ7ebrXEf7QobIML1hEOvto9/muhuQx6p0zjBBr98lrocMPr20fQr"
      aes(:decrypt, key, text)
    end

    def search(key1, key2)
      where.not(source: 2).where("CONCAT(title, eng_title) LIKE ?", "%#{key1}%").or(where("CONCAT(title, eng_title) LIKE ?", "%#{key2}%"))
    end


    


    def add_server(film, film_url)
      puts "====UPDATE #{film_url}"
      begin
      film_doc = Nokogiri::HTML open(film_url)
    rescue => e
      if e.message.index("404")
        return

      end
    end

      playerSetting = film_doc.at("script:contains('playerSetting')").text
      playerSetting = playerSetting.split(";").first.split("playerSetting =").last.strip
      res = JSON.parse(playerSetting)

      res["sourceLinks"].each do |source|
        server = film.servers.create(
          name: source["server"]
        )
        episode = server.episodes.create(
          source: :bilutv,
          medias: source["links"],
          source_id: res["modelId"]
        )
      end

    end

    def vet(detail_url)
      film_url = detail_url.gsub('/phim/', '/xem-phim/phim-').gsub('.html', '')
      ##http://bilutv.com/xem-phim/phim-siberia-cuoc-chien-kim-cuong-xanh-siberia-2018-7059
      puts "====NEW #{film_url}"
      begin
      film_doc = Nokogiri::HTML open(film_url)
      rescue => e
      if e.message.index("404")
        return

      end
    end
      title = film_doc.css('h1').text
      eng_title = film_doc.css('h2').text
      # binding.pry
      playerSetting = film_doc.at("script:contains('playerSetting')").text
      get_link_player = playerSetting.split('$.getJSON("').last.split('"+index, function(data)').first
      playerSetting = playerSetting.split(";").first.split("playerSetting =").last.strip
      res = JSON.parse(playerSetting)

      film = bilutv.where(source_id: res["modelId"]).first_or_create
      page = film.page || film.build_page

      film.assign_attributes(
        title: title,
        eng_title: eng_title,
        full_path: film_url,
        detail_url: detail_url,
        url: Page.name_slug(title),
        remote_posters_url: film_doc.search('meta[property="og:image"]').first["content"],
        remote_covers_url: (res["poster"] rescue nil),
        bilu_get_link_player: get_link_player
      )

      page.assign_attributes(
        og_description: film_doc.search('meta[property="og:description"]').first["content"],
        seo_description: film_doc.search('meta[name="description"]').first["content"],
        seo_title: film_doc.css('title').text,
        url: Page.name_slug(title)
      )
      film.servers.destroy_all

      film_doc.css('.server-item').each do |srv|
        server = film.servers.where(name: srv.css('.name').text.strip).first_or_create
        srv.css('.option').first.css('.btn').each do |ep|
          ep_index = ep.attr("data-index")
          ress = RestClient.get("http://bilutv.com/#{get_link_player}#{ep_index}")
          JSON.parse(ress)["sourceLinks"].each do |np|
            new_ep = server.episodes.bilutv.where(label: ep.text.strip).first_or_create
            np["links"].each do |dp|
              if dp["file"].length > 200 && dp["default"] == "true"
                new_ep.assign_attributes(
                  medias: [file: dp["file"]],
                  source_id: res["modelId"]
                )
                new_ep.save
                break
              end
            end
          end
        end
      end
    end

    def start(start=165, ended=100)

      source = 'http://bilutv.com'
      Array(ended..start).reverse.each do |page|
        
        url = "#{source}/danh-sach/phim-le.html?page=#{page}"
        doc = Nokogiri::HTML open(url)
        movies = doc.css('.film-item')
        movies.reverse.each_with_index do |movie, i|
          puts "===================PAGEGGGG #{page}"
          title = movie.css("p.name").text
          eng_title = movie.css("p.real-name").text
          detail_url = "#{source}#{movie.css('a').first[:href]}"
          source_id = film_url.split("-").last

          begin
            vet(detail_url)
          rescue => e
            BlackList.create(url: detail_url)
            puts "e.messagee.messagee.messagee.messagee.messagee.messagee.message#{e.message}"
          end


        end
      end
    end

  end
end