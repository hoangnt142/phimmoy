class Nationality < ApplicationRecord
  has_many :film_nationalities
  has_many :films, through: :film_nationalities
  has_one :page, as: :pageable

  after_create do
    new_page = create_page(
      url: "/#{slug}",
      title: name,
      seo_title: name,
      seo_description: name
    )
  end
end
