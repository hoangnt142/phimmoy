class Vtv16 < Film

  default_scope { vtv16 }

  def self.vtv_16(server, url, label)
    doc = Nokogiri::HTML open(url)
    logger.debug "-------------#{url}"
    b = doc.at('script:contains("urlPlay")')

    # begin
      if b
        vtv16_crawl_url = b.text.split(';').first.split('var urlPlay = ').last.gsub("'", "")
        doc2 = Nokogiri::HTML open(vtv16_crawl_url, proxy: Proxy.first.address)
        medias = doc2.at('script:contains("label")').text.split('var ')[3].gsub("sources = ", "").gsub(";\n\n    ", "").gsub("file", "url")
      elsif doc.at('script:contains("ok.ru")')
        medias = [url: doc.at('script:contains("ok.ru")').text.split("src=\"").last.split("\">").first].to_s
      end
    # rescue => e
    # end

    server.episodes.create(
      label: label,
      vtv16_crawl_url: url,
      medias: eval(medias),
      vtv16_id: url.split("-".last).last.split(".").first
    )
  end

  def self.start(url = "http://www.vtv16.com/phim/dien-hi-cong-luoc-i2-6115/tap-1-81510.html")
    doc = Nokogiri::HTML open(url)
    film_id = url.split("/")[4].split('-').last
    slug = url.split("/")[4].gsub("-#{film_id}", "")
    title = doc.css('title').text.split(' tập ').first.gsub("Xem phim ", "")
    servers = doc.css('.server')
    # Film.transaction do
      film = vtv16.find_by(source_id: film_id.to_i)
      if film.nil?
        film = vtv16.create(
          source_id: film_id,
          title: title,
          eng_title: 'Story of Yanxi Palace',
          seo_title: title,
          seo_description: doc.search('meta[name="description"]').first["content"],
          og_description: doc.search('meta[property="og:description"]').first["content"],
          film_type: 2,
          url: Page.name_slug(title),
          full_path: url
        )
        page = film.build_page
        page.title = title
        page.seo_title = title
        page.seo_description = doc.search('meta[name="description"]').first["content"]
        page.url = "/#{film.url}"
        page.save
        puts film.errors.full_messages

        servers.each do |server_doc|
          episodes = server_doc.css('.episodes').css('li')
          server = film.servers.where(name: server_doc.css('label').text).first_or_create
          episodes.each do |episode|
            ep_url = episode.css('a').first['href']
            if server.episodes.where(vtv16_crawl_url: ep_url).blank?
              label = episode.text
              vtv_16(server, ep_url, label)
            end
          end
        end

      else

        servers.each do |server_doc|
          episodes = server_doc.css('.episodes').css('li')
          server = film.servers.where(name: server_doc.css('label').text).first_or_create
          episodes.each do |episode|
            ep_url = episode.css('a').first['href']
            if server.episodes.where(vtv16_crawl_url: ep_url).blank?
              label = episode.text
              vtv_16(server, ep_url, label)
            end
          end
        end


      end


    # end
  end
end