class Server < ApplicationRecord
  has_many :episodes, dependent: :destroy
  belongs_to :film
end
