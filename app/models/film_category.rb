class FilmCategory < ApplicationRecord
  belongs_to :film
  belongs_to :category

  validates :film, uniqueness: { scope: :category }
end