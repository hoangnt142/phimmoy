class Page < ApplicationRecord
  belongs_to :pageable, polymorphic: true, optional: true

  enum publish_status: [:published, :unpublished]

  def self.name_slug(string)
    string = string.strip
    return false if !string
    unicodes = {
        'a' => ['á','à','ả','ã','ạ','ă','ắ','ặ','ằ','ẳ','ẵ','â','ấ','ầ','ẩ','ẫ','ậ','á','à'],
        'A' => ['Á','À','Ả','Ã','Ạ','Ă','Ắ','Ặ','Ằ','Ẳ','Ẵ','Â','Ấ','Ầ','Ẩ','Ẫ','Ậ'],
        'd' => ['đ'],
        'D' => ['Đ'],
        'e' => ['é','è','ẻ','ẽ','ẹ','ê','ế','ề','ể','ễ','ệ', 'ệ', 'ẹ','é','ẻ'],
        'E' => ['É','È','Ẻ','Ẽ','Ẹ','Ê','Ế','Ề','Ể','Ễ','Ệ'],
        'i' => ['í','ì','ỉ','ĩ','ị', 'ì', 'ị','ù',],
        'I' => ['Í','Ì','Ỉ','Ĩ','Ị'],
        'o' => ['ó','ò','ỏ','õ','ọ','ô','ố','ồ','ổ','ỗ','ộ','ơ','ớ','ờ','ở','ỡ','ợ','ó','ò','ỏ','õ','ọ'],
        'O' => ['Ó','Ò','Ỏ','Õ','Ọ','Ô','Ố','Ồ','Ổ','Ỗ','Ộ','Ơ','Ớ','Ờ','Ở','Ỡ','Ợ'],
        'u' => ['ú','ù','ủ','ũ','ụ','ư','ứ','ừ','ử','ữ','ự', 'ự', 'ự', 'ụ', 'ủ','ú','ù'],
        'U' => ['Ú','Ù','Ủ','Ũ','Ụ','Ư','Ứ','Ừ','Ử','Ữ','Ự'],
        'y' => ['ý','ỳ','ỷ','ỹ','ỵ'],
        'Y' => ['Ý','Ỳ','Ỷ','Ỹ','Ỵ'],
        '-' => [' - ', '---', '-–-', ' – ', ' '],
        '' => [',', '[', ']', "'",'"', '?', '/', '|', '>', '<', "”", ":",'&quot;','.', ")", "(", "!"],
      }
    unicodes.each do |non_unicode, unicode|
      unicode.each do |value|
        string.gsub!(value, non_unicode)
      end
    end
   string.strip.downcase.chomp("-")
  end

  def upload
    chunk_dir = Rails.root.join('video/split')
    token = "EAAcmwwcLjMUBAOZA1we2fQTT6I7oD1nv7ZCmXrjq8xFw24xEbDCQSkZCjwGelMKhSrZAIZBU0mdVRQObdrWUGL3FVCiXjlyoCoc4BCvRqZBzrz1WSbILxAzb3gvQIJLTMnhd6eCejt6gbJ7ZCGdhS8ZBZCY7AFc3mzT07mxrC8smxBwZDZD"
    url = "https://graph-video.facebook.com/v3.1/1602531956448562/videos"
    size = File.size(Rails.root.join("video/video2.mp4"))
    res = RestClient.post(url, {
      access_token: token,
      upload_phase: 'start',
      file_size: size
    })
    session_id = JSON.parse(res)["upload_session_id"]
    system("cd #{Rails.root.join('video/split')} && rm *")
    start_offset = JSON.parse(res)["start_offset"].to_i
    end_offset = JSON.parse(res)["end_offset"].to_i
    video_id = JSON.parse(res)["video_id"]
    puts  JSON.parse(res).to_json
    puts "VIDEO_ID--#{video_id}"
    chunks = []
    end_offset_check = 0
    success = 0
    while start_offset != size

      if success == 0
        system("cd #{Rails.root.join('video')} && split -b 1048576 video2.mp4 -d -a 3 split/aa")
      elsif success == 1
        system("cd #{Rails.root.join('video/split')} && rm aa#{(1-1).to_s.rjust(3, "0")}")
        system("cd #{Rails.root.join('video/split')} && cat aa* > video && rm aa*")
        system("cd #{Rails.root.join('video/split')} && split -b 1000000 video -d -a 3 aa")
      end


      chunk_file = File.new("#{chunk_dir}/aa#{success == 0 ? success.to_s.rjust(3, "0") : (success-1).to_s.rjust(3, "0")}", 'rb')
      puts "--START OFFSET=#{start_offset} -- CHUNK=aa#{success}/#{size/1000000}"
      res1 = RestClient.post(url, {
        access_token: token,
        upload_phase: 'transfer',
        upload_session_id: session_id.to_i,
        start_offset: start_offset,
        video_file_chunk: chunk_file
      })
      start_offset = JSON.parse(res1)["start_offset"].to_i
      end_offset = JSON.parse(res1)["end_offset"].to_i
      puts JSON.parse(res1.body).to_json
      success+=1
    end

    res2 = RestClient.post(url, {
      access_token: token,
      upload_phase: 'finish',
      upload_session_id: session_id
    })
    puts "FINISH" + JSON.parse(res2.body).to_json

  end

end
