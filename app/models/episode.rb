require 'addressable/uri'
class Episode < ApplicationRecord
  belongs_to :server
  serialize :response
  serialize :medias

  enum source: [:phimmoi, :vtv16, :bilutv, :mitdac]
  enum upload_server: [:kiidu_sg, :motsaukhong]
  # validates :medias, uniqueness: true, if: Proc.new { |a| a.medias.present? }
  belongs_to :facebook_token, optional: true

  scope :downloaded, -> {
    where(downloaded: true)
  }

  scope :undownloaded, -> {
    where(downloaded: false)
  }

  scope :has_upload, -> {
    where.not(upload_success_part: 0)
  }

  scope :no_upload, -> {
    where(upload_success_part: 0)
  }

  def fix
    doc = Nokogiri::HTML open(vtv16_crawl_url)
    b = doc.at('script:contains("vip4")')
    if b
      url2 = b.text.split(';').first.split('var urlPlay = ').last.gsub("'", "")
      doc2 = Nokogiri::HTML open(url2)
      medias = doc2.at('script:contains("label")').text.split('var ')[3].gsub("sources = ", "").gsub(";\n\n    ", "").gsub("file", "url")
    elsif doc.at('script:contains("ok.ru")')
      medias = [url: doc.at('script:contains("ok.ru")').text.split("src=\"").last.split("\">").first].to_s
    end
    self.update(
      medias: eval(medias)
    )
  end

  def film
    server.film
  end

  def upload(facebook_id=nil, chunk_size=CHUNK_SIZE)
    return if upload_finished
    # Episode.transaction do
      if upload_success_part == 0
        system("cd #{Rails.root.join('video')} && mkdir #{id}")
        system("cd #{Rails.root.join('video')}/#{id} && mkdir 'split'")
        if !downloaded
          uri = Addressable::URI.parse(video_url)
          params = uri.query_values
          params.delete('_nc_cat')
          params.delete('efg')
          params.delete('rl')
          params.delete('vabr')
          uri.query_values = params
          system("cd #{Rails.root.join('video')}/#{id} && curl '#{uri.to_s}' --output video.mp4")
        end
        @size = File.size(Rails.root.join("video/#{id}/video.mp4"))
        if @size < chunk_size
          update(video_size: @size, downloaded: false)
          system("cd #{Rails.root.join('video')} && rm -rf #{id}")
          return
        end
        facebook, video_id, upload_session_id = FacebookToken.valid(@size, facebook_id)

        update(
          video_id: video_id,
          facebook_token_id: facebook.id,
          upload_session_id: upload_session_id,
          start_offset: 0,
          video_size: @size,
          downloaded: true
        )
      else
        facebook = facebook_token
      end

      token = facebook.token
      url = "https://graph-video.facebook.com/v3.2/#{facebook.fb_page_id}/videos"
      start_offset_saved = self.start_offset.to_i
      size = @size || video_size
      chunk_dir = Rails.root.join("video/#{id}/split")
      success = upload_success_part

      while start_offset_saved != size
        if success == 0
          system("cd #{Rails.root.join('video')}/#{id} && split -b #{chunk_size} video.mp4 -d -a 4 split/aa")
          system("cd #{Rails.root.join('video')}/#{id} && rm video.mp4")
        # elsif success == 1
        #   system("cd #{Rails.root.join('video')}/#{id}/split && rm aa#{(1-1).to_s.rjust(4, "0")}")
        #   system("cd #{Rails.root.join('video')}/#{id}/split && cat aa* > video && rm aa*")
        #   system("cd #{Rails.root.join('video')}/#{id}/split && split -b #{chunk_size} video -d -a 4 aa")
        end

        chunk_path = "#{chunk_dir}/aa#{success.to_s.rjust(4, "0")}"
        chunk_file = File.open(chunk_path)
        Rails.logger.debug "------UPLOADING-#{success}/#{size/chunk_size}---"
        res1 = RestClient.post(url, {
          access_token: token,
          upload_phase: 'transfer',
          upload_session_id: self.upload_session_id.to_i,
          start_offset: start_offset_saved.to_i,
          video_file_chunk: chunk_file
        })
        Rails.logger.debug "USAGE--#{res1.headers[:x_page_usage]} -- #{facebook.page_name}"
        start_offset_saved = JSON.parse(res1)["start_offset"].to_i
        success+=1
        update_columns(
          upload_success_part: success,
          start_offset: start_offset_saved.to_s
        )
        system("rm #{chunk_path}")
      end

      res2 = RestClient.post(url, {
        access_token: token,
        upload_phase: 'finish',
        upload_session_id: self.upload_session_id
      })
      puts "FINISH" + JSON.parse(res2.body).to_json

      server.film.update(marked: true)
      update_columns(
        upload_finished: true
      )
      system("cd #{Rails.root.join('video')} && rm -rf #{id}")
    # end
  rescue => e
    puts e
  end

  def phimmoi_fix(upload_server)
    film = server.film
    res = fetch_data(film)
    res_json = JSON.parse(res.body)
    # binding.pry
    if update(video_url: res_json["medias"].first["url"], upload_server: upload_server)
      puts "GET THANH CONG #{id} #{res_json["medias"].first["url"]}"
    end
  end

  def fetch_data(film)
    cookies = eval(SystemConfig.first.cookies)
    filmslug = film.full_path.gsub('xem-phim.html', '')
    token = SystemConfig.first.token
    server = "http://episode.phimmoi.net/episodeinfo-v1.2.php?episodeid=#{source_id}&number=1&filmid=#{film.source_id}&filmslug=#{filmslug}&type=json&requestid=#{source_id}&token=#{token}"
    logger.debug "----------server url -----#{server}"
    RestClient.proxy = Proxy.first.address
    res = RestClient.get(server, timeout: 10, cookies: cookies)
    puts res.to_json
    res
  end
end
