class PageView < ApplicationRecord
    belongs_to :film
    before_create do
        self.date = Time.now 
    end
end
