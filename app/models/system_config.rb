require 'open-uri'
class SystemConfig < ApplicationRecord
  serialize :cookies
  def get_pack
    url = 'http://www.phimmoi.net/phim/cuoc-chien-bim-sua-7219/xem-phim.html'
    movie_doc = Nokogiri::HTML open(url, proxy: Rails.env.production? ? get_proxy : nil)
    pack = movie_doc.at('script:contains("fromCharCode")').text
    pack
  end

  def set_token
    res = RestClient.get("http://phimmoy.net/set-token/#{get_token}")
  end

  # def get_proxy
  #   return "http://180.148.4.194:8080"
  #   # if (Time.now - proxy_updated_at >= 1.minutes)
  #     url = "https://free-proxy-list.net/"
  #     doc = Nokogiri::HTML open(url)
  #     node = doc.at('td:contains("Vietnam")') # || doc.at('td:contains("Singapore")')
  #     host = node.parent.css('td')[0].text
  #     port = node.parent.css('td')[1].text
  #     logger.debug "-------------#{node.parent.css('td')[2].text}"
  #     update(proxy: "http://#{host}:#{port}", proxy_updated_at: Time.now)
  #   # end
  #   proxy
  # end

  def get_proxy
    Proxy.all.sample.address
  end

end
