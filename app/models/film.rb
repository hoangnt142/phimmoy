class Film < ApplicationRecord
  has_many :film_categories, dependent: :destroy
  has_many :categories, through: :film_categories

  has_many :film_nationalities, dependent: :destroy
  has_many :nationalities, through: :film_nationalities

  has_many :servers, dependent: :destroy
  has_many :film_starings, dependent: :destroy
  has_many :starings, through: :film_starings
  belongs_to :nationality, optional: true
  belongs_to :year, optional: true
  has_many :episodes, through: :servers
  has_many :page_views, dependent: :destroy
  accepts_nested_attributes_for :categories, :nationalities, :starings

  has_one :page, as: :pageable, dependent: :destroy

  enum source: [:phimmoi, :vtv16, :bilutv]
  enum film_type: { movies: 1, series: 2 }
  validates :url, uniqueness: true

  mount_uploader :posters, PosterUploader
  mount_uploader :covers, CoverUploader
  serialize :posters, JSON
  serialize :covers, JSON

  scope :published, -> {
    joins(:page).where(pages: {publish_status: Page.publish_statuses[:published]})
  }

  scope :unpublished, -> {
    joins(:page).where(pages: {publish_status: Page.publish_statuses[:unpublished]})
  }

  scope :full_search, -> (keywords) {
    sanitized_keywords = ActiveRecord::Base.connection.quote(keywords)
    where("MATCH(title) AGAINST(#{sanitized_keywords})").or(where("MATCH(eng_title) AGAINST(#{sanitized_keywords})"))
  }

  scope :unmarked, -> {
    where(marked: false).order(created_at: :DESC)
  }

  def increase_view
    page_view = page_views.where(date: Time.now.to_date).first_or_create
    page_view.update(view_count: page_view.view_count+=1)
  end

  def view_count
    page_views.sum(:view_count)
  end

  def director
    starings.where(film_starings: {is_director: true}).first || {id: 1, name: ''}
  end

  def poster
    # return "http://mitdac.tv#{posters.try(:thumb).try(:url)}" if Rails.env.development?
    bilutv? ? posters.try(:thumb).try(:url) : "http://image.phimmoi.net/film/#{source_id}/poster.thumb.jpg"    
  end

  def cover
    covers.try(:thumb).try(:url)
  end

  def self.hot_film(day_count)
    list_film = MitdacTv.joins(:page_views).select("films.*, SUM(page_views.view_count) AS total_view")
                              .where("page_views.created_at >= ?", Time.now - day_count.days)
                              .group("films.id").order("total_view DESC").limit(10).as_json(methods: [:poster])
  end
  def self.hot_film_by_type(day_count, type)
    list_film_by_type = Film.joins(:page_views).select("films.*, SUM(page_views.view_count) AS total_view")
                                      .where("film_type = ?", type)
                                      .where("page_views.created_at >= ?", Time.now - day_count.days)
                                      .group("films.id").order("total_view DESC").limit(10).as_json(methods: [:poster])                                      
  end
end
