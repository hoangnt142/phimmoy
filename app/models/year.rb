class Year < ApplicationRecord
  has_many :films
  has_one :page, as: :pageable
end
