class Phimmoi < Film

  default_scope { phimmoi }

  def self.upload(limit=100, offset=50, server='kiidu_sg', fb=8)
    ep_ids = []
    Episode.where(upload_server: nil, downloaded: false).joins(server: [:film]).where(films: {marked: false}).order(created_at: :DESC).offset(offset).limit(limit).each do |ep|
      begin
        if !ep.downloaded && ep.upload_server.nil?
          ep.phimmoi_fix(server)
          ep_ids << ep.id
        end
      rescue
        nil
      end
    end
    puts "=======================SUMMMMMM==#{ep_ids.length}"
    Episode.where(id: ep_ids).distinct.each_with_index do |ep, i|
      ep.upload fb
      puts "#{i+1}/#{ep_ids.length}"
    end
  end

  def self.fix_upload(server)
    Episode.where(upload_server: server).where(upload_finished: false).where.not(upload_success_part: 0).map {|ep| ep.upload 8 }
  end

  def self.update_info
    all_count = Film.series.where(marked: false).count
    i = 1
    Film.series.where(marked: false).where.not(id: 6324).order(id: :DESC).all.each do |film|
      # Film.transaction do
        url = "http://www.phimmoi.net/#{film.full_path}".gsub('xem-phim.html', '')
        puts "=============#{i}/--ID--#{film.id}--#{all_count} #{url}"
        doc = Nokogiri::HTML open(url)
        eng_title = doc.css('.title-2').text
        content = doc.css('.content').text
        trailer_url = doc.css('.btn-film-trailer').first.present? ? doc.css('.btn-film-trailer').first['data-videourl'] : ''
        year = Year.where(year: doc.css('.title-year').text.scan(/\d/).join('')).first_or_create

        category_ids = []
        nationality_ids = []

        categories = doc.css("a.category") || []
        nationalities = doc.css("a.country") || []
        startings = doc.css('#list_actor_carousel').css('li')

        categories.each do |cat|
          category = Category.where(name: cat.text.strip).first_or_create(slug: "/#{cat[:href]}")
          category_ids << category.id
        end

        
        nationalities.each do |nat|
          national = Nationality.where(slug: "/#{nat[:href]}").first_or_create(name: nat.text.strip)
          nationality_ids << national.id
        end


        film.update(
          eng_title: eng_title,
          content: content,
          trailer_url: trailer_url,
          year_id: year.id,
          nationality_ids: nationality_ids,
          category_ids: category_ids,
          imdb_rate: doc.css('.imdb').try(:text).try(:to_f),
          length: (doc.at("dd:contains('phút')").text rescue "")
        )

        startings.each do |star|
          id = star.css('a').first['href'].gsub('/', '').split('-').last.to_i
          name = star.css('a').css('.actor-name-a').first.text
          in_film_name = star.css('a').css('.character').first.text
          staring = Staring.where(name: name.to_s.strip).first_or_create
          film_staring = film.film_starings.where(staring_id: staring.id).first_or_create(name: in_film_name)
        end
        puts "--------------"+startings.map {|starr| starr.css('a').css('.character').first.text }.to_s
        directors = doc.css("a.director") || []
        directors.each do |dir|
          staring_id = dir['href'].gsub('/', '').split('-').last
          director = Staring.where(name: dir.text.strip).first_or_create

          film.film_starings.where(staring_id: staring_id, is_director: true).first_or_create(name: dir.text.strip)
        end
        i+= 1
        film.update(marked: true)
      end
    # end
  end

  def self.phimle(start, ended=0)

    Array(ended..start).reverse.each do |page|
      url = ended != 0 ? "http://www.phimmoi.net/phim-le/page-#{page}.html" : "http://www.phimmoi.net/phim-le/"
      doc = Nokogiri::HTML open(url)
      movies = doc.css('.movie-item')
      movies.reverse.each do |movie|
        path = movie.css('a').first['href']
        full_path = path + 'xem-phim.html'
        source_id = path.gsub('/','').split('-').last.try(:to_i)

        if !Film.find_by(id: source_id)

          movie_url = 'http://www.phimmoi.net/'+full_path
          puts "#{page} Fetching #{movie_url}"
          movie_doc = Nokogiri::HTML open(movie_url)

          
          
          string = movie_doc.at('script:contains("currentEpisode.requestId")').text
          episode_id = string[(string.index("currentEpisode.requestId='")+24)..string.index("currentEpisode.requestId='")+32].scan(/\d/).join('')
          if Episode.find_by(id: episode_id).nil?
            title = movie_doc.css('a.title-1').text
            eng_title = movie_doc.css('title-2').text
            category = Category.where(name: movie_doc.css('.breadcrumb').css('li')[2].text).first_or_create
            number = 1,
            seo_title = movie_doc.css('title').text,
            seo_description = movie_doc.search('meta[name="description"]').first["content"],
            og_description = movie_doc.search('meta[property="og:description"]').first["content"]
            Film.transaction do
              film = Film.create(
                id: source_id,
                title: title,
                eng_title: eng_title,
                full_path: full_path,
                category_id: category.id,
                number: number,
                seo_title: seo_title,
                seo_description: seo_description,
                og_description: og_description,
                movie_type_id: 1
              )
              if movie_doc.css('ul.server-list').present?
                servers = movie_doc.css('ul.server-list').css('li.backup-server')
                servers.each_with_index do |server, i|
                  ser = film.servers.create(
                    name: server.css('.server-title').text
                  )
                  ep_id = server.css('.episode').first.css('a').first['data-episodeid']
                  ser.episodes.create(
                    id: ep_id
                  )
                end
              else
                ser = film.servers.create(name: 'Vietsub')
                ser.episodes.create(
                  id: episode_id
                )
              end
            end
          end
        end
      end
    end
  end

  def self.fix_link_source(start=99, ended=1)
    Array(ended..start).reverse.each do |pg|
      url = "http://www.phimmoi.net/phim-le/page-#{pg}.html"
      doc = Nokogiri::HTML open(url)
      movies = doc.css('.movie-item')
      movies.reverse.each do |movie|
        path = movie.css('a').first['href']
        full_path = path + 'xem-phim.html'
        title = movie.css('.movie-title-1').text.strip
        source_id = path.gsub('/','').split('-').last.try(:to_i)
        film = Film.find_by(url: Page.name_slug(title))

        if film.present?
          puts "PAGE=#{pg}-FILM-#{film.id}"
          film.update(full_path: full_path,
          source: :phimmoi,
          source_id: source_id)
        end

      end
    end
  end

  def self.start(start=1, ended=1, film_type=1)
    Array(ended..start).reverse.each do |pg|
      url = "http://www.phimmoi.net/phim-#{film_type == 2 ? 'bo' : 'le'}/page-#{pg}.html"
      doc = Nokogiri::HTML open(url, proxy: Proxy.first.address)
      movies = doc.css('.movie-item')
      movies.reverse.each do |movie|
        path = movie.css('a').first['href']
        full_path = path + 'xem-phim.html'
        source_id = path.gsub('/','').split('-').last.try(:to_i)


          movie_url = 'http://www.phimmoi.net/'+full_path
          puts "#{pg} Fetching #{movie_url} #{source_id}"

          movie_doc = Nokogiri::HTML open(movie_url, proxy: Proxy.first.address)
          crawl_url = ""
          title = movie_doc.css('a.title-1').text
          eng_title = movie_doc.css('.title-2').text
          number = 1,
          seo_title = movie_doc.css('title').text,
          seo_description = movie_doc.search('meta[name="description"]').first["content"],
          og_description = movie_doc.search('meta[property="og:description"]').first["content"]

          

          Film.transaction do
           
            film = Film.where(url: Page.name_slug(title)).first_or_create(
              title: title,
              eng_title: eng_title,
              full_path: full_path,
              number: number,
              seo_title: seo_title,
              seo_description: seo_description,
              og_description: og_description,
              film_type: film_type,
              source_id: source_id,
              source: 0,
              url: Page.name_slug(title),
              remote_posters_url: "http://image.phimmoi.net/film/#{source_id}/poster.thumb.jpg",
              filled_info: false
            )
            puts film.errors.full_messages
            page = film.page || film.create_page(url: "/#{Page.name_slug(title)}")
            page.update(
              title: title,
              seo_title: seo_title,
              seo_description: seo_description,
              og_description: og_description,
              publish_status: 0
            )

            if film_type == 2
              
              servers = movie_doc.css('.server-group')
              servers.each_with_index do |server, i|
                ser = film.servers.where(name: server.css('.server-name').text).first_or_create

                episodes = server.css('.list-episode').css('.episode')
                episodes.each_with_index do |episode, ii|
                  ep_id = episode.css('a')[0].attr('data-episodeid')
                  ser.episodes.phimmoi.where(source_id: ep_id).first_or_create
                end
              end
            else
              episode_ids = []


              if movie_doc.css('ul.server-list').present?
                servers = movie_doc.css('ul.server-list').css('li.backup-server')
                servers.each_with_index do |server, i|
                  ser = film.servers.where(
                    name: server.css('.server-title').text
                  ).first_or_create
                  ep_id = server.css('.episode').first.css('a').first['data-episodeid']
                  ser.episodes.phimmoi.where(
                    source_id: ep_id
                  ).first_or_create
                  episode_ids << ep_id.to_i
                end
              else
                ser = film.servers.where(name: 'Vietsub').first_or_create
                string = movie_doc.at('script:contains("currentEpisode.requestId")').text
                episode_id = string[(string.index("currentEpisode.requestId='")+24)..string.index("currentEpisode.requestId='")+32].scan(/\d/).join('')
                ser.episodes.phimmoi.where(
                  source_id: episode_id
                ).first_or_create
                episode_ids << episode_id.to_i
              end
              film.episodes.each do |ep|
                if episode_ids.index(ep.source_id.to_i).nil?
                  puts "======================== CO DELETE ================================="
                  ep.destroy
                end
              end

            end
          
          end

      end
    end
  end
end