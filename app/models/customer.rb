class Customer < ApplicationRecord
  has_many :codes, dependent: :destroy

  enum marked: [:unmarked, :markedd]
end
