class Category < ApplicationRecord
  has_many :film_categories, dependent: :destroy
  has_many :films, through: :film_categories
  has_one :page, as: :pageable, dependent: :destroy

  after_create do
    new_page = create_page(
      url: "/#{slug}",
      title: name,
      seo_title: name,
      seo_description: name
    )
  end
end
