class FilmNationality < ApplicationRecord
  belongs_to :film
  belongs_to :nationality
end
