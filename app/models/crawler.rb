class Crawler < Film

  def self.vtv_16(server, url, label)
    doc = Nokogiri::HTML open(url)
    logger.debug "-------------#{url}"
    b = doc.at('script:contains("urlPlay")')

    # begin
      if b
        vtv16_crawl_url = b.text.split(';').first.split('var urlPlay = ').last.gsub("'", "")
        doc2 = Nokogiri::HTML open(vtv16_crawl_url, proxy: Proxy.first.address)
        medias = doc2.at('script:contains("label")').text.split('var ')[3].gsub("sources = ", "").gsub(";\n\n    ", "").gsub("file", "url")
      elsif doc.at('script:contains("ok.ru")')
        medias = [url: doc.at('script:contains("ok.ru")').text.split("src=\"").last.split("\">").first].to_s
      end
    # rescue => e
    # end

    server.episodes.create(
      label: label,
      vtv16_crawl_url: url,
      medias: eval(medias),
      vtv16_id: url.split("-".last).last.split(".").first
    )
  end

  def self.vtv16film(url = "http://www.vtv16.com/phim/dien-hi-cong-luoc-i2-6115/tap-1-81510.html")
    doc = Nokogiri::HTML open(url)
    film_id = url.split("/")[4].split('-').last
    slug = url.split("/")[4].gsub("-#{film_id}", "")

    episodes = doc.css('.episodes').css('li')
    # Film.transaction do
      if vtv16.find_by(source_id: film_id.to_i).nil?
        film = vtv16.create(
          source_id: film_id,
          title: episodes.first.css('a').first.attr('title').split(' Tập 1').first,
          eng_title: 'Story of Yanxi Palace',
          seo_title: doc.css('title').text,
          seo_description: doc.search('meta[name="description"]').first["content"],
          og_description: doc.search('meta[property="og:description"]').first["content"],
          movie_type_id: 2,
          category_id: 16,
          year_id: 53
        )
        puts film.errors.full_messages
        full_path = "/#{slug}-#{film.id}/xem-phim.html"
        film.update(full_path: full_path)
        server = film.servers.create(name: doc.css('.server').css('label').text)
        episodes.each do |episode|
          ep_url = episode.css('a').first['href']
          label = episode.text
          vtv_16(server, ep_url, label)
        end
      end
    # end
  end

  def self.update_info
    all_count = Film.where(marked: false).count
    i = 1
    Film.series.where(marked: false).find_each do |film|
      transaction do
        url = "http://www.phimmoi.net/#{film.full_path}".gsub('xem-phim.html', '')
        puts "=============#{i}/#{all_count} #{url}"
        doc = Nokogiri::HTML open(url)
        eng_title = doc.css('.title-2').text
        content = doc.css('.content').text
        trailer_url = doc.css('.btn-film-trailer').first.present? ? doc.css('.btn-film-trailer').first['data-videourl'] : ''
        year = Year.where(year: doc.css('.title-year').text.scan(/\d/).join('')).first_or_create

        category_ids = []
        nationality_ids = []

        categories = doc.css("a.category") || []
        nationalities = doc.css("a.country") || []
        startings = doc.css('#list_actor_carousel').css('li')


        categories.each do |cat|
          category = Category.where(name: cat.text.strip).first_or_create(slug: "/#{cat[:href]}")
          category_ids << category.id
        end

        
        nationalities.each do |nat|
          national = Nationality.where(name: nat.text.strip).first_or_create(slug: "/#{nat[:href]}")
          nationality_ids << national.id
        end


        film.update(
          eng_title: eng_title,
          content: content,
          trailer_url: trailer_url,
          year_id: year.id,
          nationality_ids: nationality_ids,
          category_ids: category_ids,
          imdb_rate: doc.css('.imdb').try(:text).try(:to_f),
          length: (doc.at("dd:contains('phút')").text rescue "")
        )

        startings.each do |star|
          id = star.css('a').first['href'].gsub('/', '').split('-').last.to_i
          name = star.css('a').css('.actor-name-a').first.text
          in_film_name = star.css('a').css('.character').first.text
          staring = Staring.where(id: id).first_or_create(name: name)
          film_staring = film.film_starings.where(staring_id: staring.id).first_or_create(name: in_film_name)
        end
        puts "--------------"+startings.map {|starr| starr.css('a').css('.character').first.text }.to_s
        directors = doc.css("a.director") || []
        directors.each do |dir|
          staring_id = dir['href'].gsub('/', '').split('-').last
          director = Staring.where(id: staring_id).first_or_create(name: dir.text.strip)

          film.film_starings.where(staring_id: staring_id, is_director: true).first_or_create(name: dir.text.strip)
        end
        i+= 1
        film.update(marked: true)
      end
    end
  end

  def self.phim(path)
    full_path = path + 'xem-phim.html'
    source_id = path.gsub('/','').split('-').last.try(:to_i)
    movie_url = 'http://www.phimmoi.net/'+full_path
    puts "#{page} Fetching #{movie_url}"
    movie_doc = Nokogiri::HTML open(movie_url, proxy: SystemConfig.first.get_proxy)


    string = movie_doc.at('script:contains("currentEpisode.requestId")').text
    episode_id = string[(string.index("currentEpisode.requestId='")+24)..string.index("currentEpisode.requestId='")+32].scan(/\d/).join('')
    if Episode.find_by(id: episode_id).nil?
      title = movie_doc.css('a.title-1').text
      eng_title = movie_doc.css('.title-2').text
      number = 1,
      seo_title = movie_doc.css('title').text,
      seo_description = movie_doc.search('meta[name="description"]').first["content"],
      og_description = movie_doc.search('meta[property="og:description"]').first["content"]
      Film.transaction do
        film = Film.phimmoi.create(
          source_id: source_id,
          title: title,
          eng_title: eng_title,
          full_path: full_path,
          number: number,
          seo_title: seo_title,
          seo_description: seo_description,
          og_description: og_description,
          url: Page.name_slug(title)
        )
        if movie_doc.css('ul.server-list').present?
          servers = movie_doc.css('ul.server-list').css('li.backup-server')
          servers.each_with_index do |server, i|
            ser = film.servers.create(
              name: server.css('.server-title').text
            )
            ep_id = server.css('.episode').first.css('a').first['data-episodeid']
            ser.episodes.create(
              id: ep_id
            )
          end
        else
          ser = film.servers.create(name: 'Vietsub')
          ser.episodes.create(
            id: episode_id
          )
        end
      end
    end

  end


  def self.phimle(start, ended=0)

    Array(ended..start).reverse.each do |page|
      url = ended != 0 ? "http://www.phimmoi.net/phim-le/page-#{page}.html" : "http://www.phimmoi.net/phim-le/"
      doc = Nokogiri::HTML open(url)
      movies = doc.css('.movie-item')
      movies.reverse.each do |movie|
        path = movie.css('a').first['href']
        

        if !Film.find_by(source_id: source_id)

          phim(path)

        end
      end
    end
  end

  def self.phimbo(start=79, ended=2)
    # client = Selenium::WebDriver::Remote::Http::Default.new
    # client.read_timeout = 20
    # browser = Watir::Browser.new
    Array(ended..start).reverse.each do |page|
      url = ended != 0 ? "http://www.phimmoi.net/phim-bo/page-#{page}.html" : "http://www.phimmoi.net/phim-bo/"
      doc = Nokogiri::HTML open(url)
      movies = doc.css('.movie-item')
      movies.reverse.each do |movie|
        path = movie.css('a').first['href']
        full_path = path + 'xem-phim.html'
        source_id = path.gsub('/','').split('-').last.try(:to_i)

        if Film.phimmoi.find_by(id: source_id.to_i).nil?
          movie_url = 'http://www.phimmoi.net/'+full_path
          puts "#{page} Fetching #{movie_url} #{source_id}"

          movie_doc = Nokogiri::HTML open(movie_url)
          crawl_url = ""
          title = movie_doc.css('a.title-1').text
          eng_title = movie_doc.css('title-2').text
          category = Category.where(name: movie_doc.css('.breadcrumb').css('li')[2].text).first_or_create
          number = 1,
          seo_title = movie_doc.css('title').text,
          seo_description = movie_doc.search('meta[name="description"]').first["content"],
          og_description = movie_doc.search('meta[property="og:description"]').first["content"]
          # string = movie_doc.at('script:contains("currentEpisode.requestId")').text
          # episode_id = episode_id = string[(string.index("currentEpisode.requestId='")+24)..string.index("currentEpisode.requestId='")+32].scan(/\d/).join('')
          Film.transaction do
            film = Film.phimmoi.new(
              title: title,
              eng_title: eng_title,
              full_path: full_path,
              number: number,
              seo_title: seo_title,
              seo_description: seo_description,
              og_description: og_description,
              film_type: 2,
              source_id: source_id,
              url: Page.name_slug(title)
            )
            if movie_doc.css('.server-group').present? && film.save
              servers = movie_doc.css('.server-group')
              servers.each_with_index do |server, i|
                ser = film.servers.create(
                  name: server.css('.server-name').text
                )

                episodes = server.css('.list-episode').css('.episode')
                episodes.each_with_index do |episode, ii|
                  ep_id = episode.css('a')[0].attr('data-episodeid')
                  if Episode.find_by(id: ep_id).nil?
                    ser.episodes.create(
                      id: ep_id
                    )
                  end
                end
              end
            end
          end
        end
      end
    end
  end

end