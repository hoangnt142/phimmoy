class CoverUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  storage :file
  def store_dir
    "uploads/films/#{mounted_as}/#{model.id}"
  end

  version :thumb do
    process resize_to_fill: [600, 350]
  end

end
