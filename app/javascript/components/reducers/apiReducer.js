import { actions } from '../actions/apiActions'

const initialState = {
  films: [],
  film: {},
  top_hot_films: [],
  hot_series_film: [],
  hot_feature_film: [],
  servers: [],
  episode: {},
  episodeId: null,
  fetching: false,
  total_pages: 1,
  current_page: 1,
  isFetching: true,
  page: null
}

const apiReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.API_REQUEST_BEGIN:
      return Object.assign({}, state, action.isFetching)
    case actions.API_REQUEST_END:
      return Object.assign({}, state, action.isFetching)      
    case actions.API_RECEIVED:
      const receivedParams = action.json
      return Object.assign({}, state, 
        {
          ...receivedParams,
          isFetching: false,
        })
    case actions.API_RESET:
      switch (action.name) {
        case 'films':
          return Object.assign({}, state, {
            films: [],
            total_pages: 1,
            current_page: 1,
            page: null
          })
        case 'film':
          return Object.assign({}, state, {
            film: {},
            servers: [],
            episode: {},
            page: null
          })
        case 'episode':
          return Object.assign({}, state, {
            episode: {}
          })
        case 'page':
          return Object.assign({}, state, {
            page: null
          })
        default:
          return Object.assign({}, initialState)
      }
    default:
      return state
  }
}

export default apiReducer
