import { combineReducers } from 'redux'

import apiReducer from './apiReducer'
import playerReducer from './playerReducer'

const reducers = combineReducers({
  'api': apiReducer,
  'player': playerReducer
})

export default reducers