import { actions } from '../actions/playerActions'

const initialState = {
  sources: [],
  playlist: [{sources:[]}],
  changed: true,
  fixing: false,
  fixingTime: 0
}

const playerReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.SET_PLAYLIST:
      return Object.assign({}, state, {
        playlist: action.playlist,
        changed: !state.changed,
        fixing: false,
        fixingTime: state.fixingTime += 1
      })
    case actions.FIXING:
      return Object.assign({}, state, {
        fixing: true
      })
    case actions.PLAYER_RESET:
      return Object.assign({}, state, {
        playlist: initialState.playlist,
        fixingTime: 0
      })
    default:
      return state
  }
}

export default playerReducer
