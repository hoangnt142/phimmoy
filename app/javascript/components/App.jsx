import React, { PureComponent } from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'

import configureStore from './store/store'
import routes from './routes/routes'


export default class App extends PureComponent {

  constructor(props) {
    super(props)
    this.store = configureStore(this.props)
  }

  render() {
    return (
      <Provider store={this.store}>
        <Router>
          {routes()}
        </Router>
      </Provider>
    )
  }
}
