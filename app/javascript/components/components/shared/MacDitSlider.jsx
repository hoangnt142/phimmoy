import React, { Fragment } from "react"
import PropTypes from "prop-types"

class MacDitSlider extends React.Component {

  constructor(props) {
    super(props);
    this.slider = React.createRef();
  }

  static defaultProps = {
    speed: 5
  }

  next = () => {
    const { itemWidth, speed } = this.props
    self = this;
    let scrollAmount = 0
    const distance = itemWidth
    let slideTimer = setInterval(function(){
      self.slider.current.scrollLeft += speed
      scrollAmount += speed;
      if(scrollAmount >= distance){
          window.clearInterval(slideTimer);
      }
    }, 10);

  }

  prev = () => {
    const { itemWidth, speed } = this.props
    self = this;
    let scrollAmount = 0
    const distance = itemWidth
    let slideTimer = setInterval(function(){
      self.slider.current.scrollLeft -= speed
      scrollAmount += speed;
      if(scrollAmount >= distance){
          window.clearInterval(slideTimer);
      }
    }, 10);
  }

  render() {
    const { children, numItem, itemWidth } = this.props
    return (
      <div className='hori-slide'>
        <div className='list-slider' ref={this.slider}>
          <div className="item-wrap" style={{width: numItem*itemWidth}}>
            {children}
          </div>
        </div>
        <div className='next' onClick={() => this.next()}>❯</div>
        <div className='prev' onClick={() => this.prev()}>❮</div>
      </div>
    )
  }
}

export default MacDitSlider
