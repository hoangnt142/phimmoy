import React, { PureComponent } from 'react'
import PropType from 'prop-types'
import { Link } from 'react-router-dom'
import Responsive from 'react-responsive'

export default class Pagination extends PureComponent {

  static contextTypes = {
    router: PropType.object.isRequired,
  }

  static defaultProps = {
    maxBlocks: 7,
    paginationRange: []
  }

  buildPageArray = () => {
    const {
      totalPages,
      currentPage,
      maxBlocks,
    } = this.props
    const pages = []
    if(totalPages >= maxBlocks) {
      if(currentPage < (maxBlocks - 2)) {
        for(let i = 1; i <= (maxBlocks - 2); i++) {
          pages.push(i)
        }
        pages.push(null)
        pages.push(totalPages)
      }
      else if(currentPage > (totalPages - (maxBlocks - 2) + 1)) {
        pages.push(1)
        pages.push(null)
        for(let i = (totalPages - (maxBlocks - 2) + 1); i <= totalPages; i++) {
          pages.push(i)
        }
      }
      else {
        pages.push(1)
        pages.push(null)
        for(let i = (currentPage - 1); i <= (currentPage + 1); i++) {
          pages.push(i)
        }
        pages.push(null)
        pages.push(totalPages)
      }
    }

    return pages
  }

  render() {
    const {
      totalPages,
      currentPage,
      totalCount,
      paginationRange,
    } = this.props

    const basePath = this.context.router.route.location.pathname.split("/page/")[0]

    const pages = this.buildPageArray()
    const firstPageLink = basePath
    const previousPageLink = Math.max(currentPage - 1, 1) > 1 ? `${basePath}/page/${Math.max(currentPage - 1, 1)}` : basePath
    const hasPrevious = currentPage > 1
    const nextPageLink = Math.min(currentPage + 1, totalPages) > 1 ? `${basePath}/page/${Math.min(currentPage + 1, totalPages)}` : basePath
    const hasNext = currentPage < totalPages
    const lastPageLink = totalPages > 1 ? `${basePath}/page/${totalPages}` : basePath

    return (
      <div>
        <div
          aria-label="Pagination Navigation"
          className="pagination menu"
          role="navigation"
        >
          <Link to={firstPageLink}
                aria-current="false"
                tabIndex="0"
                value="1"
                type="firstItem">
            <div>
              <i aria-hidden="true"></i>Đầu tiên
            </div>
          </Link>
          <Link to={previousPageLink}
                aria-current="false"
                tabIndex="0"
                value={currentPage > 1 ? currentPage - 1 : 1}
                rel='prev'
                type="prevItem">
            <div><i aria-hidden="true" className="chevron left icon"></i>Trước</div>
          </Link>
          {pages.map((p, i) => {
             if(p) {
               return <Link key={i}
                            to={p > 1 ? `${basePath}/page/${p}` : basePath}
                            aria-current="false"
                            tabIndex="0"
                            value={p}
                            type="pageItem"
                            className={"item" + (p == currentPage ? ' active':'')}>{p}</Link>
             }
             else {
               return <a key={i}
                         aria-current="false"
                         tabIndex="-1"
                         type="ellipsisItem"
                         className="disabled icon item">
                 ...
               </a>
             }
          })}
          <Link to={nextPageLink}
                aria-current="false"
                tabIndex="0"
                value={currentPage < totalPages ? currentPage + 1 : totalPages}
                rel='next'
                type="nextItem"
                className="item"
          >
            <div>Tiếp<i aria-hidden="true" className="chevron right icon"></i></div>
          </Link>
          <Link to={lastPageLink}
                aria-current="false"
                tabIndex="0"
                value={totalPages}
                type="lastItem"
                className="item">
            <div className="ui text blue">Cuối<i aria-hidden="true" className="chevron right icon"></i></div>
          </Link>

        </div>
      </div>
    )
  }
}
