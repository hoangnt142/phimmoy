import React, { Fragment } from "react"
import PropTypes from "prop-types"
import { Link } from 'react-router-dom'

class FilmList extends React.Component {

  render() {
    const { films } = this.props
    return (
      <ul className="movies">
        {films.map(film =>
          <li key={film.id} className="movie">
            <Link to={`/phim/${film.url}`}>
              <div className="poster">
                <img src={film.poster} />
              </div>
            </Link>
            <div className="info">
              <Link to={`/phim/${film.url}`}><div className="title">{film.title}</div>
                <div className="eng-title">{film.eng_title}</div>
              </Link>
            </div>
          </li>
        )}
      </ul>
    )
  }
}

export default FilmList