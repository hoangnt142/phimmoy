import React from "react"
import PropTypes from "prop-types"
import Responsive from 'react-responsive'
import Tab from './Tab'
import SearchForm from './SearchForm'
import MacDitSlider from './MacDitSlider'
import { Link } from 'react-router-dom'

class Banner extends React.PureComponent {
  state = {
    isButtonActive: 1
  }
  componentDidMount() {
    let banner = document.getElementsByClassName('banner')[0]
    banner.style.background = `url(/images/banner${Math.floor((Math.random() * 2) + 1)}.jpg) no-repeat`
    banner.style.backgroundSize = 'cover'
  }

  changeColorButton = (buttonNumber) => {
    const { apiGet } = this.props;
    apiGet(`/hotphim/${buttonNumber}`)    
    this.setState({
      isButtonActive: buttonNumber
    });
  }

  render() {
    const { movies, series, suggest_films, topHotMovie } = this.props

    return (
      <React.Fragment>
        <div className="banner">
          <div className="container">
            <Responsive minWidth={992} as='div' className='sliders'>
              <div className='hot'>
                <MacDitSlider numItem={suggest_films.length} itemWidth={659.09} speed={15}>
                  {suggest_films.map(film =>
                    <div className='hot-item' key={film.id}>
                      <Link to={`/phim/${film.url}`}>
                        <img src={`http://mitdac.tv${film.cover}`} />
                      </Link>
                      <div className='info'>
                        <Link to={`/phim/${film.url}`}>
                          <div className="title">{film.title}</div>
                          <div className="eng-title">{film.eng_title}</div>
                        </Link>
                      </div>
                    </div>
                  )}
                </MacDitSlider>
              </div>
              <div className='hot-movies-title'>
              <ul>
                <li 
                  className={"active"}
                  className={this.state.isButtonActive === 1 ? 'active' : null}
                  onClick={() => {
                    this.changeColorButton(1);
                }}>
                  PHIM LẺ HOT
                </li>
                <li
                  className={this.state.isButtonActive === 7 ? 'active' : null}
                  onClick={() => {
                    this.changeColorButton(7);
                }}>
                  TUẦN
                </li>
                <li
                  className={this.state.isButtonActive === 30 ? 'active' : null}
                  onClick={() => {
                    this.changeColorButton(30);
                  }}>
                  THÁNG
                </li>
              </ul>
                
              </div>
              <MacDitSlider numItem={10} itemWidth={170}>
                {topHotMovie.slice(0,10).map(film =>
                  <div key={film.id} className='movie'>
                    <Link to={`/phim/${film.url}`}>
                      <img style={{width: 160}} src={film.poster} />
                    </Link>
                    <div className='info'>
                      <Link to={`/phim/${film.url}`}>
                        <div className="title">{film.title}</div>
                        <div className="eng-title">{film.eng_title}</div>
                      </Link>
                    </div>
                  </div>
                )}
              </MacDitSlider>
            </Responsive>
            <Responsive maxWidth={767}>
              <SearchForm keyword='' />
            </Responsive>
            <Tab movies={movies} series={series}/>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default Banner
