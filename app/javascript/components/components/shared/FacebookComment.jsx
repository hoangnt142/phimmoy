import React, { PureComponent } from "react"

export default class FacebookComment extends PureComponent {

  componentWillMount() {
    this.initializeFacebookSDK()
  }

  initializeFacebookSDK() {
    if (process.env.NODE_ENV == 'production') {
      const googleApiKey = process.env.GOOGLE_API

      let scripts = document.getElementsByTagName("script")

      scripts = Array.from(scripts).filter(script => script.id.indexOf('facebook-jssdk') > -1)

      if (scripts.length == 0) {
        let s = document.createElement('script')
        s.type = 'text/javascript'
        s.id = 'facebook-jssdk'
        s.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.1&appId=285664295394682&autoLogAppEvents=1'
        if (document.body.appendChild(s) != null) {
          // setTimeout(() => {FB.XFBML.parse()}, 500);
        }
      } else {
        setTimeout(() => {FB.XFBML.parse()}, 0);
      }
    }
  }

  render() {
    if (process.env.NODE_ENV == 'production') { return null }
    const urlFacebookBox = location.href.replace("/xem-phim/", "/phim/")
    return (
      <div className="container main-show col grid fb-comments">
        <div className="fb-comments left col-7 hello" data-href={urlFacebookBox} data-colorscheme="dark" data-width="100%" data-numposts="5"></div>
        <div className="right col-3"></div>
      </div>
    )
  }
}
