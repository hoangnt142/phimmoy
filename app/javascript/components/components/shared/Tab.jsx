import React, { Fragment } from "react"
import PropTypes from "prop-types"
import Responsive from 'react-responsive'
import { Link } from 'react-router-dom'

class Tab extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      active: true,
      currentList: null
    }
  }

  activate = (active) => {
    this.setState({
      active: active,
      currentList: active ? this.props.movies : this.props.series
    })
  }

  render() {
    const { movies } = this.props
    const { active, currentList } = this.state
    return (
      <div className='new-movies'>
        <div className='new-movies-header'>
          <div className={active ? 'tab active' : 'tab'} onClick={() => this.activate(true)}>
            PHIM LẺ MỚI
          </div>
          <div className={active ? 'tab' : 'tab active'} onClick={() => this.activate(false)}>
            PHIM BỘ MỚI
          </div>
        </div>
        <div className='new-movies-content'>
          <ul>
            {!movies ?
              <Fragment>
                {[...Array(10)].map((x, i) =>
                  <li key={i}>
                    <div className='info'>
                      <a href='#'>
                        <div className='poster-loading animated-background'></div>
                        <div className="title">
                          <div className='vi-title animated-background' style={{width: Math.floor(Math.random()*(300-180+1)+180)}}></div>
                          <div className='eng-title animated-background' style={{width: Math.floor(Math.random()*(300-180+1)+180)}}></div>
                        </div>
                      </a>
                    </div>
                  </li>
                )}
              </Fragment>
              :
              <Fragment>
                {(currentList || movies).map((film, i) =>
                  i < 10 &&
                  <li key={film.id}>
                    <div className='info'>
                      <Link to={`/phim/${film.url}`}>
                        <div className="film-image" style={{backgroundImage: `url(${film.poster})`}}></div>
                        <div className="title">
                          <div className='vi-title'>{film.title}</div>
                          <div className='eng-title'>{film.eng_title}</div>
                        </div>
                      </Link>
                    </div>

                  </li>
                )}
              </Fragment>
            }
          </ul>
        </div>
      </div>
    )
  }
}

export default Tab
