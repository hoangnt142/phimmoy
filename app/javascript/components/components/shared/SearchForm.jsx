import React from "react"
import PropTypes from "prop-types"
import { params } from 'lib/common_function'

class SearchForm extends React.Component {

  static contextTypes = {
    router: PropTypes.object.isRequired,
  }

  search = () => {
    console.log(this.input.value)
    const location = {
      pathname: `/tim-kiem/${this.input.value}`
    }
    this.context.router.history.push(location)
  }

  render() {
    return (
      <div className="search">
        <input
          ref={(input) => this.input = input}
          placeholder="Tìm phim"
          type="search"
          defaultValue={params().keyword}
          onKeyPress={(e) => {
            if (e.key === 'Enter') {
              this.search()
            }
          }}
        />
        <button className="button" onClick={() => this.search()}>Tìm</button>
      </div>
    )
  }
}

export default SearchForm
