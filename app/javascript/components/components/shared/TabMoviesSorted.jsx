import React, { Component, Fragment } from 'react';
import ListHotFilm from './ListHotFilm';

// @TODO: CHANGE INTO INLINE STYLE
class TabMoviesSorted extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isButtonActive: 1
    }
  }

  changeColorButton = (buttonNumber) => {
    const { apiGet, typeFilm } = this.props;
    apiGet(`/hotphim/${buttonNumber}/${typeFilm}`)
    this.setState({
      isButtonActive: buttonNumber
    });
  }
  renderTabFilm = () =>
    <div className="box-content">
      <div className='hot-movies-title' >
        <ul>
          <li
            className={"active"}
            className={this.state.isButtonActive === 1 ? 'active' : null}
            onClick={() => {
              this.changeColorButton(1);
            }}>
            NGÀY
          </li>
          <li
            className={this.state.isButtonActive === 7 ? 'active' : null}
            onClick={() => {
              this.changeColorButton(7);
            }}>
            TUẦN
          </li>
          <li
            className={this.state.isButtonActive === 30 ? 'active' : null}
            onClick={() => {
              this.changeColorButton(30);
            }}>
            THÁNG
          </li>
        </ul>
      </div>
      <ListHotFilm listFilm={this.props.top_hot_films} />
    </div>
  render() {
    return (
      <Fragment>
        <h2>{this.props.typeFilm === "1" ? "Phim Lẻ" : "Phim Bộ"} XEM NHIỀU</h2>
        {this.renderTabFilm()}
      </Fragment>
    )
  }
};

export default TabMoviesSorted;