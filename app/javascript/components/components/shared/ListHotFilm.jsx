import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'
import ErrorImg from "../../../../assets/images/error404.jpg";
 const ListHotFilm = (props) => {
  const  { listFilm } = props;
  if(!listFilm) 
    return <div>{null}</div>;
  return (
    <Fragment>
      <ul className="list-hot-films">
        {listFilm.map(film => (
          <li key={film.title}>
          <div className='info'>
            <Link to={`/phim/${film.url}`} >
            <div className="box-left" style={{backgroundImage: `url(${film.poster})`}}></div>
              <div className="box-right">
                <span className="vn-name">{film.title}</span>
                <span className="eng-name">{film.eng_title}</span>
                <span className="view-count">Lượt xem: {film.total_view}</span>
              </div>
            </Link>
            </div>
          </li>
        ))}
      </ul>
    </Fragment>
  )
}

export default ListHotFilm;