import React, { Fragment } from "react"
import PropTypes from "prop-types"
import { Link } from 'react-router-dom'
import TabMoviesSorted from './shared/TabMoviesSorted';
import FacebookComment from './shared/FacebookComment'
import Player from '../containers/PlayerContainer'

export default class Watching extends React.Component {

  render() {
    console.log('watching')
    const { film, hot_feature_film, hot_series_film, apiGet, episodeId, servers, episode, isFetching } = this.props

    const { match } = this.context.router.route
    return (
      <Fragment>
        <div className="container main-show col grid watching">
          <div className="left col-7">
            <div className="movie-detail">
              <div className="poster">
                <img src={film.poster} />
              </div>
              <div className="info">
                <h1>
                  {film.title}
                  <span>{film.eng_title}</span>
                </h1>
                <div className="film-description-box">
                <dl>
                  {
                    film.content ? (
                      film.content.length < 250 ?
                        <dd className="film-content">{film.content}</dd> :
                          (<dd>{film.content.substring(0,250)+"..."}[<Link to={`/phim/${film.url}`}>Read more</Link>]</dd>)
                    ) :
                    <dd className="film-content">...[<Link to={`/phim/${film.url}`}>Read more</Link>]</dd>
                  }
                  </dl>
                </div>
              </div>
            </div>
            <Player servers={servers}/>
          </div>
          <div className="right col-3">
            <div className="box">
            <TabMoviesSorted
                typeFilm="1"
                top_hot_films={hot_feature_film}
                apiGet={apiGet}
              />
              <TabMoviesSorted
                typeFilm="2"
                top_hot_films={hot_series_film}
                apiGet={apiGet}
              />
            </div>
          </div>
        </div>
        <FacebookComment />
      </Fragment>
    )
  }
}
Watching.contextTypes = {
  router: PropTypes.object.isRequired,
}