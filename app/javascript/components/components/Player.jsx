import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

export default class Player extends React.Component {

  constructor(props, context) {
    super(props)
  }

  getEpisode = (epId) => {
    const { reset, getEpisode } = this.props
    reset('episode')
    getEpisode(epId)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.pack != this.props.pack) {
      let unpackOutput = unpack(nextProps.pack)
      let token = unpackOutput.split(";")[0].replace("eval('console.log(\"","").replace('")','')
      this.props.setToken(token)
    }
  }

  componentDidUpdate() {
    const { fixLink, playlist, fixing, fixingTime } = this.props
    if (document.getElementById('player') && !fixing) {
      if (playlist[0].sources[0]) {
        let player = jwplayer('player')
        player.setup({
          playlist: playlist,
          width: '100%',
          height: 400
        });
        player.on('error', (event) => {
          if (fixingTime < 3) {
            fixLink()
          }
        });
      }
    }
  }

  render() {
    const {
      servers,
      episode,
      getEpisode,
      playlist,
      fixLink,
      changed,
      fixing
    } = this.props
    const { history, route } = this.context.router
    if(process.env.NODE_ENV != 'production') {
      console.log(this.props)
      console.log('playList', playlist)
      console.log('Router router router', this.context)
      console.log('episode', episode)
    }
    return (
      <Fragment>
        <div className='player'>
          {fixing &&
            <div className='loader'>
              <div className='loading-icon'></div>
              Đang lấy link dự phòng
            </div>
          }
          <div id='player' />
        </div>
        <div className='controls'>
          <div className='download'>
            Download
          </div>
          <div className='download'>
            Tắt đèn
          </div>
          <div className='download'>
            Phóng to
          </div>
        </div>
        <div className='servers'>
          {servers.map(server =>
            <div className='server' key={server.id}>
              <div className='server-name'>{server.name}</div>
              <div className='episodes'>
                {server.episodes.map((ep, i) => 
                  <div className={episode.id == ep.id ? 'active episode' : 'episode'} key={i}>
                    <Link
                      to={{ pathname: route.location.pathname, search: `?ep=${ep.id}`}}
                      onClick={() => this.getEpisode(ep.id)}
                    >{i+1}</Link>
                  </div>
                )}
              </div>
            </div>
          )}
        </div>
      </Fragment>
    )
  }
}
Player.contextTypes = {
  router: PropTypes.object.isRequired,
}