import React, { Fragment } from "react"
import PropTypes from "prop-types"
import Responsive from 'react-responsive'
import MacDitSlider from './shared/MacDitSlider'
import { Link } from 'react-router-dom'
import TabMoviesSorted from './shared/TabMoviesSorted';
import FacebookComment from './shared/FacebookComment'
import ErrorImg from "../../../assets/images/error404.jpg";

class Show extends React.Component {

  render() {
    const { film, viewCount, hot_feature_film, hot_series_film, apiGet, isFetching  } = this.props;
    return (
      <Fragment>
        <div className="container main-show col grid">
          <div className="left col-7">
            <Responsive as='div' maxWidth={640}>
              <h1>
                {film.title}
                <span>{film.eng_title}</span>
              </h1>
            </Responsive>
            <div className="info-wrap">
              <div className="poster">
              <div className="movie-img">
                {
                  film.poster ? <img src={film.poster} /> : <img src={ErrorImg} />
                }
                <Link to={`/xem-phim/${film.url}`} className='dangerous'>Xem Phim</Link>
              </div>
              </div>
              <div className="info">
                <Responsive as='div' minWidth={641}>
                  <h1>
                    {film.title}
                    <span>{film.eng_title}</span>
                  </h1>
                </Responsive>
                <div className="summary">
                  <dl>
                    <dt>Điểm IBDM: </dt>
                    <dd>{film.imdb_rate}</dd>
                  </dl>
                  <dl>
                    <dt>Lượt xem: </dt>
                    <dd>{film.view_count}</dd>
                  </dl>
                  <dl>
                    <dt>Đạo diễn: </dt>
                    <dd>{ film.director ? <Link to={`/star/${film.director.id}`}>{film.director.name}</Link> : null }</dd>
                  </dl>
                  <dl>
                    <dt>Quốc gia: </dt>
                    <dd>
                      {film.nationalities && film.nationalities.map(nationality =>
                        <Link key={nationality.id} title={`Phim ${nationality.name}`} to={nationality.slug}>{nationality.name}</Link>
                      ).reduce((accu, elem) => {
                        return accu === null ? [elem] : [...accu, ', ', elem]
                      }, null)}
                    </dd>
                  </dl>
                  <dl>
                    <dt>Năm: </dt>
                    <dd>{film.year ? film.year.year : ''}</dd>
                  </dl>
                  <dl>
                    <dt>Thể loại: </dt>
                    <dd>
                      {film.categories && film.categories.map(category => {
                        return (<Link key={category.id} title={category.name} to={category.slug}>{category.name}</Link>)
                      }).reduce((accu, elem) => {
                        return accu === null ? [elem] : [...accu, ', ', elem]
                      }, null)}
                    </dd>
                  </dl>
                  <dl>
                    <dt>Thời lượng: </dt>
                    <dd className='film-content'>{film.length}</dd>
                  </dl>
                </div>
              </div>
            </div>
            <div className="content-film">
              <h2>Nội dung</h2>
              <div className="content-text">
                <p>{film.content}</p>
              </div>
            </div>
            <div className="staring">
              <h2>DIỄN VIÊN</h2>
              <div className="stars">
                {film.starings &&
                  <MacDitSlider numItem={film.starings.length} itemWidth={100}>
                    {film.starings.map(star =>
                      <Link to={`/star/${star.id}`} className='star' key={star.id} style={{ width: 90 }}>
                        <div
                          className='star-avatar'
                          style={{
                            width: 90,
                            backgroundImage: `url(http://image.phimmoi.net/profile/${star.id}/thumb.jpg) , url(http://image.phimmoi.net/profile/default/male/thumb.jpg)`
                          }}
                        />
                        <div className='star-name'>{star.name}</div>
                        <div className="in-film-name">{film.film_starings.find(f_star => f_star.film_id == film.id).in_film_name}</div>
                      </Link>
                    )}
                  </MacDitSlider>
                }
              </div>
            </div>
          </div>
          <div className="right col-3">
            <div className="box">
              <TabMoviesSorted
                typeFilm="1"
                top_hot_films={hot_feature_film}
                apiGet={apiGet}
              />
              <TabMoviesSorted
                typeFilm="2"
                top_hot_films={hot_series_film}
                apiGet={apiGet}
              />
            </div>
          </div>
        </div>
        <FacebookComment />
      </Fragment>
    )
  }
}

export default Show