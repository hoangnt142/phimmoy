import React, { Component } from "react";

const Spinner = props => {
  return (
    <div className="ipl-progress-indicator" id="ipl-progress-indicator">
      <div className="ipl-progress-indicator-head">
        <div className="first-indicator"></div>
        <div className="second-indicator"></div>
      </div>
      <div className="loading-wrapper">
        <div className="loading-text">LOADING</div>
        <div className="loading-content"></div>
      </div>
    </div>
  )
}

export default Spinner;