import React, { Fragment } from "react"
import PropTypes from "prop-types"
import Banner from './shared/Banner'
import FilmList from './shared/FilmList'
import Pagination from './shared/Pagination'

class Films extends React.Component {

  render() {
    const { films, total_pages, current_page } = this.props
    return (
      <div className='container main'>
        <FilmList films={films} />
        {total_pages > 1 && <Pagination totalPages={total_pages} currentPage={current_page} /> }
      </div>
    )
  }
}

export default Films