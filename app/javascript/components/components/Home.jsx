import React, { Fragment } from "react"
import PropTypes from "prop-types"
import Banner from './shared/Banner'
import FilmList from './shared/FilmList'

class Home extends React.Component {

  render() {
    const { series, movies, suggest_films, topHotMovie } = this.props.api
    const { apiGet } = this.props

    return (
      <Fragment>
        <Banner
          series={series}
          movies={movies}
          suggest_films={suggest_films}
          topHotMovie={topHotMovie}
          apiGet={apiGet}          
        />
        <div className="container main">
          <h2>PHIM LẺ MỚI</h2>
          <FilmList films={movies} />
          <h2>PHIM BỘ MỚI</h2>
          <FilmList films={series} />
        </div>
      </Fragment>
    )
  }
}

export default Home