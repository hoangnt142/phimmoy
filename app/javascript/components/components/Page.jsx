import React, { Component } from 'react'
import PropType from 'prop-types'

import Home from '../containers/HomeContainer'
import Show from '../containers/ShowContainer'
import Watching from '../containers/WatchingContainer'
import Films from '../containers/FilmsContainer'

const Spinner = props => {
  return (
    <div className="ipl-progress-indicator" id="ipl-progress-indicator">
      <div className="ipl-progress-indicator-head">
        <div className="first-indicator"></div>
        <div className="second-indicator"></div>
      </div>
      <div className="loading-wrapper">
        <div className="loading-text">LOADING</div>
        <div className="loading-content"></div>
      </div>
    </div>
  )
}

export default class PageContainer extends Component {

  constructor(props) {
    super(props)
    const { fetchPage } = this.props
    fetchPage()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { location, fetchPage, reset } = this.props
    if(prevProps.location.pathname != location.pathname) {
      reset()
      window.scrollTo(0, 0)
      fetchPage()
    }
  }

  render() {
    const { page, location } = this.props
    if (!page) {
      return <Spinner />
    } else {
      switch(page.pageable_type) {
        case 'Film':
          if (location.pathname.indexOf('/xem-phim/') > -1) {
            return (<Watching />)
          } else {
            return (<Show />)
          }
        case 'Index':
          return (<Home />)
        case 'ListFilm':
          return (<Films />)
        case 'Category':
          return (<Films />)
        default:
          return (<Spinner />)
      }
    }
  }
}
