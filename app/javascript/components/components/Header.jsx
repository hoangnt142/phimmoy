import React from "react"
import PropTypes from "prop-types"
import Responsive from 'react-responsive'
import Banner from './shared/Banner'
import SearchForm from './shared/SearchForm'
import { Link } from 'react-router-dom'

class MenuItem extends React.PureComponent {

  state = {open: false}

  toggle = () => {
    this.setState({
      open: !this.state.open
    })
  }

  render() {
    const {menuItem} = this.props
    const {open} = this.state
    return (
      <li className={!menuItem.no_dropdown ? 'dropdown' : 'no-dropdown'}>
        <Link to={menuItem.url}>
          {menuItem.name}
        </Link>
        {!menuItem.no_dropdown &&
          <span onClick={() => this.toggle()} style={{transform: open ? "rotate(180deg)" : "rotate(0deg)"}}>▾</span>
        }
        {!menuItem.no_dropdown &&
          <div className={open ? 'dropdown-list active' : 'dropdown-list'} style={{height: open ? menuItem.items.length*47 : 0}}>
            <ul>
              {menuItem.items.map((item,ii) =>
                <li key={ii}><Link to={item.url}>{item.title}</Link></li>
              )}
            </ul>
          </div>
        }
      </li>
    )
  }
}

class MobileMenu extends React.PureComponent {

  state = {open: false}

  toggle = () => {
    this.setState({
      open: !this.state.open
    })
  }

  render() {
    const { menu } = this.props;
    const { open } = this.state;
    return (
      <React.Fragment>
        <div onClick={() => this.toggle()} style={{position: 'fixed',top: 20, right: 15, zIndex: 100000}}>
          <img style={{cursor: 'pointer'}} src='/images/menu.svg' />
        </div>
        <nav id="menu-responsive" className={open ? 'active' : ''}>
          <ul className="nav" role="tablist">
            {menu.map((menuItem, i) =>
              <MenuItem key={i} menuItem={menuItem} />
            )}
          </ul>
        </nav>
      </React.Fragment>
    )
  }
}

export default class Header extends React.PureComponent {

  render() {
    const { films } = this.props
    console.log('render')
    return (
      <header>
        <div className="container">
          <Link to="/"><img className="logo" src="/images/logo-md.svg" /></Link>
          <Responsive as='div' className='nav' minWidth={992}>
            <div>
              <ul>
                {menu.map((menu, i) =>
                  <li key={i} className={!menu.no_dropdown ? 'dropdown' : 'no-dropdown'}>
                    <Link to={menu.url}>
                      {menu.name}
                      {!menu.no_dropdown &&
                        <span>▾</span>
                      }
                    </Link>
                    {!menu.no_dropdown &&
                      <div className='dropdown-list'>
                        <ul>
                          {menu.items.map((item,ii) =>
                            <li key={ii}><Link to={item.url}>{item.title}</Link></li>
                          )}
                        </ul>
                      </div>
                    }
                  </li>
                )}
              </ul>
            </div>
          </Responsive>
          <Responsive minWidth={768}>
            <SearchForm />
          </Responsive>
        </div>
        <Responsive maxWidth={767}>
          <MobileMenu menu={menu} />
        </Responsive>
      </header>
    )
  }
}
const categories = "[{\"title\":\"Phim hành động\",\"url\":\"/the-loai/phim-hanh-dong\"},{\"title\":\"Phim võ thuật\",\"url\":\"/the-loai/phim-vo-thuat\"},{\"title\":\"Phim bộ\",\"url\":\"/the-loai/phim-bo\"},{\"title\":\"Phim hình sự\",\"url\":\"/the-loai/phim-hinh-su\"},{\"title\":\"Phim tâm lý\",\"url\":\"/the-loai/phim-tam-ly\"},{\"title\":\"Phim thuyết minh\",\"url\":\"/the-loai/phim-thuyet-minh\"},{\"title\":\"Phim gia đình\",\"url\":\"/the-loai/phim-gia-dinh\"},{\"title\":\"Phim thần thoại\",\"url\":\"/the-loai/phim-than-thoai\"},{\"title\":\"Phim tình cảm-Lãng mạn\",\"url\":\"/the-loai/phim-tinh-cam-lang-man\"},{\"title\":\"Phim viễn tưởng\",\"url\":\"/the-loai/phim-vien-tuong\"},{\"title\":\"Phim hoạt hình\",\"url\":\"/the-loai/phim-hoat-hinh\"},{\"title\":\"Phim hài hước\",\"url\":\"/the-loai/phim-hai-huoc\"},{\"title\":\"Phim cổ trang\",\"url\":\"/the-loai/phim-co-trang\"},{\"title\":\"Phim phiêu lưu\",\"url\":\"/the-loai/phim-phieu-luu\"},{\"title\":\"Phim hồi hộp-Gây cấn\",\"url\":\"/the-loai/phim-hoi-hop-gay-can\"},{\"title\":\"Phim Bí ẩn-Siêu nhiên\",\"url\":\"/the-loai/phim-bi-an-sieu-nhien\"},{\"title\":\"Phim kinh điển\",\"url\":\"/the-loai/phim-kinh-dien\"},{\"title\":\"Phim lẻ\",\"url\":\"/the-loai/phim-le\"},{\"title\":\"Phim kinh dị\",\"url\":\"/the-loai/phim-kinh-di\"},{\"title\":\"Phim chiếu rạp\",\"url\":\"/the-loai/phim-chieu-rap\"},{\"title\":\"Phim chiến tranh\",\"url\":\"/the-loai/phim-chien-tranh\"},{\"title\":\"Phim Thể thao-Âm nhạc\",\"url\":\"/the-loai/phim-the-thao-am-nhac\"},{\"title\":\"Phim tài liệu\",\"url\":\"/the-loai/phim-tai-lieu\"},{\"title\":\"Phim chính kịch - Drama\",\"url\":\"/the-loai/phim-chinh-kich-drama\"}]"
const nationalities = JSON.parse("[{\"title\":\"Âu - Mỹ\",\"url\":\"au-my\"},{\"title\":\"Nhật Bản\",\"url\":\"nhat-ban\"},{\"title\":\"Hồng Kông\",\"url\":\"hong-kong\"},{\"title\":\"Trung Quốc\",\"url\":\"trung-quoc\"},{\"title\":\"Hàn Quốc\",\"url\":\"han-quoc\"},{\"title\":\"Đài Loan\",\"url\":\"dai-loan\"},{\"title\":\"Thái Lan\",\"url\":\"thai-lan\"},{\"title\":\"Anh\",\"url\":\"anh\"}]")
const menu = [
  {
    name: 'TRANG CHỦ',
    url: '/',
    no_dropdown: true
  },
  {
    name: 'THỂ LOẠI',
    url: '#',
    items: JSON.parse(categories)
  },
  {
    name: 'QUỐC GIA',
    url: '#',
    items: nationalities.map(nat => ({"title": nat.title, "url": `/quoc-gia/${nat.url}`}) )
  },
  {
    name: 'PHIM LẺ',
    url: '/phim-le',
    items: [
      {
        title: 'Phim lẻ 2018',
        url: '/phim-le/2018'
      },
      {
        title: 'Phim lẻ 2017',
        url: '/phim-le/2017'
      },
      {
        title: 'Phim lẻ 2016',
        url: '/phim-le/2016'
      },
      {
        title: 'Phim lẻ 2015',
        url: '/phim-le/2015'
      },
      {
        title: 'Phim lẻ 2014',
        url: '/phim-le/2015'
      },
      {
        title: 'Phim lẻ 2013',
        url: '/phim-le/2015'
      },
      {
        title: 'Phim lẻ 2012',
        url: '/phim-le/2012'
      },
      {
        title: 'Phim lẻ trước 2012',
        url: '/phim-le/truoc-2012'
      }
    ]
  },
  {
    name: 'PHIM BỘ',
    url: '/phim-bo',
    items: nationalities.map(nat => ({title: nat.title, url: `/phim-bo/${nat.url}`}))
  }
]
