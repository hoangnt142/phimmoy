import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Header from '../components/Header'
import { AsyncComponent } from '../components/Bundle'

import ScrollTop from '../components/ScrollTop'
import Watching from '../containers/WatchingContainer'
import PageContainer from '../containers/PageContainer'

const Films = (props) => {
  return AsyncComponent(() => import('../containers/FilmsContainer'), props)
}
const Show = (props) => {
  return AsyncComponent(() => import('../containers/ShowContainer'), props)
}
// const Watching = (props) => {
//   return asyncComponent(() => import('../containers/WatchingContainer'), props)
// }
const Page = (props) => <PageContainer {...props} />

export default () => {
  let context = null
  const getContext = () => context

  return (
    <div>
      <Route path="/" component={() => <Header />} />
      <div ref={_context => context = _context }>
        <Switch>
          <Route exact path="/" component={Page} />
          {/*<Route exact path="/" component={HomeContainer} />
          <Route exact path="/phim-bo" component={Films} />
          <Route path="/phim-bo/:url" component={Films} />

          <Route exact path="/phim-le" component={Films} />
          <Route path="/phim-le/:url" onUpdate={() => alert(1)} component={Films} />

          <Route path="/quoc-gia/:url" component={Films} />
          <Route path="/the-loai/:url" component={Films} />

          <Route path="/tim-kiem/:keyword" component={Films} />

          <Route path="/phim/:url" component={Show} />

          <Route path="/xem-phim/:url/:ep_id" component={Watching} />
          <Route path="/xem-phim/:url" component={Watching} />*/}
          <Route path="/:url+/page/:page" component={Page} />
          <Route path="/xem-phim/:url" component={Page} />
          <Route path="/:url+" component={Page} />
          
        </Switch>
      </div>
      {/*<Route path="/" exact component={() => <PageFooter showNav={false} />} />*/}
    </div>
  )
}
