export const actions = {
  API_RECEIVED: 'API_RECEIVED',
  API_FAILED: 'API_FAILED',
  API_REQUEST_BEGIN: 'API_REQUEST_BEGIN',
  API_REQUEST_END: 'API_REQUEST_END',
  API_RESET: 'API_RESET'
}
import { setPlaylist, reset as playerRest } from './playerActions'

export const receive = (json) => (dispatch, getState) => {
  dispatch({
    type: actions.API_RECEIVED,
    json
  })
  if (json.episode) {
    dispatch(setPlaylist())
  }
}

export const requestBegin = () => (dispatch) => {
  dispatch({
    type: actions.API_REQUEST_BEGIN,
    isFetching: true,
  })
}

export const requestEnd = () => (dispatch) => {
  dispatch({
    type: actions.API_REQUEST_END,
    isFetching: false,
  })
}


export const reset = (name) => (dispatch) => {
  dispatch(playerRest())
  dispatch({
    type: actions.API_RESET,
    name: name
  })
}

export const fetchPage = () => {
  const path = window.location.href.replace(/^http?:\/\/[a-z0-9:\._-]+/, '')
  const url = `/pages${path}`
  return apiGet(url)
}

export const apiGet = (url,query='') => {
  const defaultInit = {
    method: 'GET',
    mode: 'same-origin',
    credentials: 'same-origin'
  }
  const mergedInit = Object.assign({}, defaultInit);
  return dispatch => {
    dispatch(requestBegin());
    return fetch(`/api${url+query}`, mergedInit)
      .then(response => {
        if(!response.ok) {
          throw `Fetch was failed: ${response.code}`
        }
        return response.json()
                       .then(json => {
                         console.log('json', json)
                         dispatch(requestEnd());
                         dispatch(receive(json))
                       })
                       .catch(error => {
                         dispatch(requestEnd());
                         console.error(error)
                       })
      })
      .catch(error => {
        console.error(error)
      })
  }
}
