import CryptoJS from 'crypto-js'
import { apiGet } from './apiActions'
import 'lib/unpacker'

export const actions = {
  SET_PLAYLIST: 'SET_PLAYLIST',
  FIXING: 'FIXING',
  PLAYER_RESET: 'PLAYER_RESET'
}

export const reset = () => (dispatch) => {
  dispatch({
    type: actions.PLAYER_RESET
  })
}

export const getEpisode = (epId) => (dispatch, getState) => {
  dispatch(apiGet(`/episodes/${epId}`))
}

export const setPlaylist = () => (dispatch, getState) => {
  const { film, episode } = getState().api
  let playlist = getState().player.playlist

  let episode_url = episode.video_url

  playlist = [
    {
      sources: [
        {
          file: episode_url.replace('video.xx', 'scontent.fvca1-1.fna'),
          label: "720p",
          type: "mp4",
          default: true
        }
      ],
      image: film.avatar_url || `http://image.phimmoi.net/film/${film.source_id}/preview.thumb.jpg`,
      title: film.title,
      description: " (Vietsub)",
      trackTitle: film.title+" /  (Vietsub)",
      trackLabel: film.title
    }
  ]
  dispatch({
    type: actions.SET_PLAYLIST,
    playlist
  })
}

export const er = (sr) => (dispatch, getState) => {
  let playlist = getState().player.playlist
  let data = sr
  let key = `bilutv.com4590481877${getState().api.film.source_id}`
  var bytes  = CryptoJS.AES.decrypt(data, key);
  var url = bytes.toString(CryptoJS.enc.Utf8);
  console.log('urlurlurlurlurlurl', url)
  window.fetch(url)
  .then(function(response) {
    return response.json();
  })
  .then(function(json) {
    playlist = [{
      sources: json
    }]
    console.log(json)
    return dispatch({
      type: actions.SET_PLAYLIST,
      playlist
    })
  })
}

export const fixLink = () => (dispatch, getState) => {
  const { episode } = getState().api
  // dispatch({
  //   type: actions.FIXING
  // })
  if (episode.id) {
    dispatch(apiGet('/fix-link/'+episode.id))
  }
}

export const setToken = (token) => (dispatch) => {
  fetch('/api/set-token/'+token)
  .then(function(response) {
    return response.json();
  })
  .then(function(json) {
    dispatch(fixLink())
  })
}
