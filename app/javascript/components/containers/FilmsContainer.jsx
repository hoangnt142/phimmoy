import { connect } from 'react-redux'
import Films from '../components/Films'
import { apiGet, reset } from '../actions/apiActions'

const mapStateToProps = (state) => ({
  films: state.api.films || [],
  total_pages: state.api.total_pages,
  current_page: state.api.current_page
})

export default connect(mapStateToProps, { apiGet, reset })(Films)
