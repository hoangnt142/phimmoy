import { connect } from 'react-redux'
import Home from '../components/Home'
import { apiGet, reset } from '../actions/apiActions'

const mapStateToProps = (state) => ({
  api: state.api,
  topHotMovie: state.api.top_hot_films
})

export default connect(mapStateToProps, { apiGet, reset })(Home)
