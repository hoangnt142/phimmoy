import { connect } from 'react-redux'
import Show from '../components/Show'
import { apiGet, reset } from '../actions/apiActions'

const mapStateToProps = (state) => ({
  film: state.api.film,
  top_hot_films: state.api.top_hot_films,
  hot_series_film: state.api.hot_series_film,
  hot_feature_film: state.api.hot_feature_film,
  isFetching: state.api.isFetching,
})

export default connect(mapStateToProps, { apiGet, reset })(Show)
