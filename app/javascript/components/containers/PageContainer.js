import { connect } from 'react-redux'
import Page from '../components/Page'
import { fetchPage, reset } from '../actions/apiActions'

const mapStateToProps = (state) => ({
  page: state.api.page || null
})

export default connect(mapStateToProps, { fetchPage, reset })(Page)
