import { connect } from 'react-redux'
import Player from '../components/Player'
import { getEpisode, fixLink, setToken } from '../actions/playerActions'
import { reset, apiGet } from '../actions/apiActions'

const mapStateToProps = (state) => ({
  pack: state.api.pack,
  film: state.api.film,
  playlist: state.player.playlist,
  episode: state.api.episode,
  changed: state.player.changed,
  fixing: state.player.fixing,
  fixingTime: state.player.fixingTime
})

export default connect(mapStateToProps, {
  getEpisode,
  reset,
  apiGet,
  fixLink,
  setToken
})(Player)
