const categories = "[{\"title\":\"Phim hành động\",\"url\":\"/the-loai/phim-hanh-dong/\"},{\"title\":\"Phim viễn tưởng\",\"url\":\"/the-loai/phim-vien-tuong/\"},{\"title\":\"Phim phiêu lưu\",\"url\":\"/the-loai/phim-phieu-luu/\"},{\"title\":\"Phim chiếu rạp\",\"url\":\"/phim-chieu-rap/\"},{\"title\":\"Phim thuyết minh\",\"url\":\"/phim-thuyet-minh/\"},{\"title\":\"Phim lẻ\",\"url\":\"/phim-le/\"},{\"title\":\"Phim võ thuật\",\"url\":\"/the-loai/phim-vo-thuat/\"},{\"title\":\"Phim kinh dị\",\"url\":\"/the-loai/phim-kinh-di/\"},{\"title\":\"Phim hình sự\",\"url\":\"/the-loai/phim-hinh-su/\"},{\"title\":\"Phim gia đình\",\"url\":\"/the-loai/phim-gia-dinh/\"},{\"title\":\"Phim tâm lý\",\"url\":\"/the-loai/phim-tam-ly/\"},{\"title\":\"Phim chiến tranh\",\"url\":\"/the-loai/phim-chien-tranh/\"},{\"title\":\"Phim cổ trang\",\"url\":\"/the-loai/phim-co-trang/\"},{\"title\":\"Phim hài hước\",\"url\":\"/the-loai/phim-hai-huoc/\"},{\"title\":\"Phim tài liệu\",\"url\":\"/the-loai/phim-tai-lieu/\"},{\"title\":\"Phim hoạt hình\",\"url\":\"/the-loai/phim-hoat-hinh/\"},{\"title\":\"Phim bộ\",\"url\":\"/phim-bo/\"},{\"title\":\"Phim Thể thao-Âm nhạc\",\"url\":\"/the-loai/phim-the-thao-am-nhac/\"},{\"title\":\"Phim hồi hộp-Gây cấn\",\"url\":\"/the-loai/phim-hoi-hop-gay-can/\"},{\"title\":\"Phim kinh điển\",\"url\":\"/phim-kinh-dien/\"},{\"title\":\"Phim thần thoại\",\"url\":\"/the-loai/phim-than-thoai/\"},{\"title\":\"Phim tình cảm-Lãng mạn\",\"url\":\"/the-loai/phim-tinh-cam-lang-man/\"},{\"title\":\"Phim Bí ẩn-Siêu nhiên\",\"url\":\"/the-loai/phim-bi-an-sieu-nhien/\"},{\"title\":\"Phim chính kịch - Drama\",\"url\":\"/the-loai/phim-chinh-kich-drama/\"}]"
const nationalities = JSON.parse("[{\"title\":\"Âu - Mỹ\",\"url\":\"au-my\"},{\"title\":\"Nhật Bản\",\"url\":\"nhat-ban\"},{\"title\":\"Hồng Kông\",\"url\":\"hong-kong\"},{\"title\":\"Trung Quốc\",\"url\":\"trung-quoc\"},{\"title\":\"Hàn Quốc\",\"url\":\"han-quoc\"},{\"title\":\"Đài Loan\",\"url\":\"dai-loan\"},{\"title\":\"Thái Lan\",\"url\":\"thai-lan\"},{\"title\":\"Anh\",\"url\":\"anh\"}]")
export default [
  {
    name: 'TRANG CHỦ',
    url: '/',
    no_dropdown: true
  },
  {
    name: 'THỂ LOẠI',
    url: '#',
    items: JSON.parse(categories)
  },
  {
    name: 'QUỐC GIA',
    url: '#',
    items: nationalities.map(nat => ({"title": nat.title, "url": `/quoc-gia/${nat.url}`}) )
  },
  {
    name: 'PHIM LẺ',
    url: '/phim-le',
    items: [
      {
        title: 'Phim lẻ 2018',
        url: '/phim-le/2018'
      },
      {
        title: 'Phim lẻ 2017',
        url: '/phim-le/2017'
      },
      {
        title: 'Phim lẻ 2016',
        url: '/phim-le/2016'
      },
      {
        title: 'Phim lẻ 2015',
        url: '/phim-le/2015'
      },
      {
        title: 'Phim lẻ 2014',
        url: '/phim-le/2015'
      },
      {
        title: 'Phim lẻ 2013',
        url: '/phim-le/2015'
      },
      {
        title: 'Phim lẻ 2012',
        url: '/phim-le/2012'
      },
      {
        title: 'Phim lẻ trước 2012',
        url: '/phim-le/truoc-2012'
      }
    ]
  },
  {
    name: 'PHIM BỘ',
    url: '/phim-bo',
    items: nationalities.map(nat => ({title: nat.title, url: `/phim-bo/${nat.url}`}))
  }
]