class Api::PagesController < ApiController

  def index
    prefix = request.path.index('xem-phim/') ? 'watching_' : ''
    @page = Page.find_by(url: current_path)
    eval("render_#{prefix}#{@page.pageable_type.downcase}")
  end

  def render_index
    movies = MitdacTv.movies.order(created_at: :DESC).limit(20)
    series = MitdacTv.series.order(created_at: :DESC).limit(20)
    suggest_films = MitdacTv.movies.where.not(covers: nil).order(created_at: :DESC).limit(10)
    render json: {
      page: @page,
      movies: movies.as_json(only: [:id, :title, :eng_title, :url, ], methods: [:poster]),
      series: series.as_json(only: [:id, :title, :eng_title, :url, ], methods: [:poster]),
      suggest_films: suggest_films.as_json(only: [:id, :title, :eng_title, :url, ], methods: [:cover]),
      topHotMovie: Film.hot_film(1)
    }
  end

  def render_film
    film = @page.pageable
    film.increase_view
    render json: {
      page: @page,
      film: film.as_json(
        methods: [:poster, :categories, :director, :year, :nationalities, :starings, :film_starings, :view_count]
      ),
      hot_series_film: Film.hot_film_by_type(1, "2"),
      hot_feature_film: Film.hot_film_by_type(1, "1")
    }
  end

  def render_watching_film
    film = @page.pageable
    film.increase_view 
    servers = film.servers.includes(:episodes)
    render json: {
      page: @page,
      film: film.as_json(
        methods: [:poster]
      ),
      servers: servers.as_json(
        only: [:id, :name],
        include: [{
          episodes: {
            only: [:id]
          }
        }]
      ),
      episode: servers.first.episodes.where(upload_finished: true).first,
      hot_series_film: Film.hot_film_by_type(1, "2"),
      hot_feature_film: Film.hot_film_by_type(1, "1")
    }
  end

  def category
    films = MitdacTv.joins(:film_categories).where(film_categories: {category_id: params[:id].to_i}).order("films.source_id DESC").page(params[:page]).per(30)
    # category = Category.find(params[:id])
    # films = category.films.where(marked: true).order(source_id: :DESC).page(params[:page]).per(30)
    render json: {
      page: @page,
      films: films.as_json(methods: [:poster]),
      total_pages: films.total_pages,
      current_page: films.current_page
    }
  rescue
    raise ActionController::RoutingError.new('Not Found')
  end

  def nationality
    nat = Nationality.find(params[:id])
    if params[:series]
      films = MitdacTv.series.joins(:film_nationalities).where(film_nationalities: {nationality_id: params[:id]}).order(id: :DESC).page(params[:page]).per(30)
    else
      films = MitdacTv.joins(:film_nationalities).where(film_nationalities: {nationality_id: params[:id]}).order(id: :DESC).page(params[:page]).per(30)
    end
    render json: {
      page: @page,
      films: films.as_json(methods: [:poster]),
      total_pages: films.total_pages,
      current_page: films.current_page
    }
  rescue
    raise ActionController::RoutingError.new('Not Found')
  end

  def year
    y = Year.find_by(year: params[:year])
    if y
      films = y.films.movies.order(id: :DESC).page(params[:page]).per(30)
    elsif params[:all]
      films = MitdacTv.movies.order(id: :DESC).page(params[:page]).per(30)
    else
      films = MitdacTv.movies.where(year_id: Year.where.not(year: [*2012..2018]).pluck(:id))
                .order(id: :DESC)
                .page(params[:page]).per(30)
    end
    render json: {
      page: @page,
      films: films.as_json(methods: [:poster]),
      total_pages: films.total_pages,
      current_page: films.current_page
    }
  rescue
    raise ActionController::RoutingError.new('Not Found')
  end

  def phimbo
    films = MitdacTv.series.order(id: :DESC).page(params[:page]).per(30)
    render json: {
      page: @page,
      films: films.as_json(methods: [:poster]),
      total_pages: films.total_pages,
      current_page: films.current_page
    }
  end

end