class Api::FilmsController < ApiController
  skip_before_action :get_page, only: [:top_film, :top_hot_films, :hot_film_by_type]
  def show
    film = Film.find_by(url: params[:url])
    film.increase_view 
    servers = film.servers.includes(:episodes)
    render json: {
      film: film.as_json(
        methods: [:poster, :categories, :director, :year, :nationalities, :starings, :film_starings, :view_count]
      ),
      servers: servers.as_json(
        only: [:id, :name],
        include: [{
          episodes: {
            only: [:id]
          }
        }]
      ),
      episode: params[:ep_id] == 'undefined' ? servers.first.episodes.first : {},
    }
  end

  def top_film
    days = params[:days].to_i
    render json: {
      top_hot_films: Film.hot_film(days)
    }
  end

  def hot_film_by_type
    days = params[:days].to_i
    type = params[:type].to_s
    render json: {
      top_hot_films: Film.hot_film_by_type(days, type)
    }
  end

  def servers
    film = Film.find(params[:film_id])
    servers = film.servers.includes(:episodes)
    render json: {
      servers: servers.as_json(
        only: [:id, :name],
        include: [{
          episodes: {
            only: [:id]
          }
        }]
      ),
      episode: params[:ep_id] == 'null' ? servers.first.episodes.first : {}
    }
  end

  def search
    keywords = params[:keyword].to_s.strip
    films = Film.full_search(keywords).page(params[:page]).per(30)
    render json: {
      films: films.as_json(methods: [:poster])
    }
  end

  def phimmoi
    path = params[:phimmoi_path]
    Crawler.phim(path)
    render json: {success: true}
  rescue
    render json: {dmm: true}
  end

  def vtv16
    path = params[:vtv16_path]
    Vtv16.start(path)
    render json: {success: true}
  rescue
    render json: {dmm: true}
  end
end