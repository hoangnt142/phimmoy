class Api::EpisodesController < ApiController

  def show
    episode = Episode.find(params[:id])
    render json: {episode: episode, episodeId: episode.id}
  end

  def fix_link
    episode = Episode.find_by(id: params[:episode_id])
    if episode.vtv16_id.nil?
      facebook_token = episode.facebook_token
      access_token = facebook_token.token
      # binding.pry
      
      res = RestClient.get("https://graph.facebook.com/v3.1/#{episode.video_id}?access_token=#{access_token}&fields=source")
      res = JSON.parse(res)
      # binding.pry
      episode.update(video_url: res["source"])
      # film = episode.server.film
      # res = fetch_data(episode.source_id, film)
      # res_json = JSON.parse(res.body)
      # if res_json['status'] == 0
      #   return render json: {pack: system_config.get_pack}
      # end
      # media = episode.update(response: res_json, medias: res_json["medias"])
    else
      episode.fix
    end
    render json: {episode: episode}
  end

  def set_token
    system_config.update(token: params[:token])
    render json: {ok: true}
  end

end