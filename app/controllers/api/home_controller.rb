class Api::HomeController < ApiController

  def get_index_data
    movies = MitdacTv.movies.order(created_at: :DESC).limit(20)
    series = MitdacTv.series.order(created_at: :DESC).limit(20)
    suggest_films = MitdacTv.movies.where.not(covers: nil).order(created_at: :DESC).limit(10)
    render json: {
      movies: movies.as_json(only: [:id, :title, :eng_title, :url, ], methods: [:poster]),
      series: series.as_json(only: [:id, :title, :eng_title, :url, ], methods: [:poster]),
      suggest_films: suggest_films.as_json(only: [:id, :title, :eng_title, :url, ], methods: [:cover])
    }
  end

  def customer
    customer = Customer.unmarked.joins(:codes).where(codes: {code_string: params[:code].rjust(4, '0')}).first
    customer.update(marked: 1)
    render json: {customer: customer}
  end

  def get_codes
    codes = Code.joins(:customer).where(customers: {marked: 0})
    render json: {codes: codes.pluck(:code_string)}
  end

  def reset
    Customer.update_all(marked: 0)
  end

end