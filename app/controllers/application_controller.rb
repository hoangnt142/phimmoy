class ApplicationController < ActionController::Base

  before_action :set_domain, :get_page

  rescue_from ActiveRecord::RecordNotFound,     with: :render_404
  rescue_from ActionController::RoutingError,   with: :render_404

  def render_404
    respond_to do |format|
      format.html do
        render 'errors/error_404', status: 404
        return
      end

      format.any do
        render nothing: true, status: 404
        return
      end
    end
  end

  def set_domain
    if request.domain == 'phimmoy.net'
      redirect_to "http://mitdac.tv#{request.path}"
    end
  end

  def get_page
    page_url = current_path
    @page = Page.find_by(url: page_url)
    if @page.nil?
      return render json: {error: 'Not Found'}, status: 404
    end
    if request.path.index('/xem-phim/')
      @page.title = "Xem phim #{@page.title.downcase}"
    end
  end

  def current_path
    regexp_match_page = /\/page\/([0-9]+)\/?$/
    regexp_match_ep = /\/ep-([0-9]+)\/?$/
    request.path.gsub(regexp_match_page, '').gsub(regexp_match_ep, "").gsub('/phim/', '/').gsub('/xem-phim/', '/').gsub('/api/pages', '/').gsub('//', '/')
  end

  def system_config
    SystemConfig.first
  end
end
