class ApiController < ApplicationController
  skip_before_action :get_page, if: -> { params[:controller] == 'api/episodes' }
end