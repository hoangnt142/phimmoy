class Admin::HomeController < AdminController
  def index
    @facebooks = FacebookToken.all
    @system_config = SystemConfig.first
  end

  def update_info
    params[:facebook].each do |id, data|
      fb = FacebookToken.find(id)
      fb.update(
        fb_page_id: data[:fb_page_id],
        token: data[:token]
      )
    end
    # binding.pry
    SystemConfig.first.update(
      token: params[:config][:token],
      cookies: params[:config][:cookies]
    )
    redirect_to '/admin'
  end
end