class PagesController < ApplicationController

  def category
    category = Category.find(params[:id])
    @films = category.films.order(id: :DESC).page(params[:page]).per(30)
    render "home/films"
  rescue
    raise ActionController::RoutingError.new('Not Found')
  end

  def nationality
    nat = Nationality.find(params[:id])
    if params[:series]
      @films = nat.films.series.order(id: :DESC).page(params[:page]).per(30)
    else
      @films = nat.films.order(id: :DESC).page(params[:page]).per(30)
    end
    render "home/films"
  rescue
    raise ActionController::RoutingError.new('Not Found')
  end

  def year
    y = Year.find_by(year: params[:year])
    if y
      @films = y.films.movies.order(id: :DESC).page(params[:page]).per(30)
    else
      @films = Film.movies.where(year_id: Year.where.not(year: [*2012..2018]).pluck(:id))
                .order(id: :DESC)
                .page(params[:page]).per(30)
    end
    render "home/films"
  rescue
    raise ActionController::RoutingError.new('Not Found')
  end

end