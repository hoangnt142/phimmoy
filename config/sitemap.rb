# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://mitdac.tv/"

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  add root_path, changefreq: 'monthly'

  Page.all.each do |page|
    if page.pageable_type == "Film"
      add "/phim#{page.url}", changefreq: "monthly", lastmod: page.updated_at
      add "/xem-phim#{page.url}", changefreq: "monthly", lastmod: page.updated_at      
    else
      add "#{page.url}", changefreq: "monthly", lastmod: page.updated_at
    end 
  end
end
