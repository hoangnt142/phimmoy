Rails.application.routes.draw do
  root to: 'home#index'

  get '/quayso/haianh' => 'random#haianh'

  namespace :admin do
    get '/' => 'home#index'
    post '/update-info' => 'home#update_info'
  end

  namespace :api do
    get 'pages', to: 'pages#index'
    get '/index-data' => 'home#get_index_data'

    get '/crawler/phimmoi' => 'films#phimmoi'
    get '/crawler/vtv16' => 'films#vtv16'

    get '/phim/:url' => 'films#show'

    get '/hotphim/:days' => 'films#top_film'
    
    get '/hotphim/:days/:type' => 'films#hot_film_by_type'

    get '/xem-phim/:url(/:ep_id)' => 'films#show'

    get '/tim-kiem/:keyword(/page/:page)' => 'films#search'

    get '/films/servers/:film_id' => 'films#servers'

    get '/episodes/:id' => 'episodes#show'

    get '/fix-link/:episode_id' => 'episodes#fix_link'
    get '/set-token/:token' => 'episodes#set_token'

    

    get '/phim-bo(/page/:page)' => 'pages#phimbo'


    get 'customer' => 'home#customer'
    get 'get-codes' => 'home#get_codes'
    get 'customer-reset' => 'home#reset'

    scope :pages do

      Category.all.each do |cat|
        get "/the-loai/#{cat.slug}(/page/:page)", to: 'pages#category', id: cat.id
      end

      Nationality.where(id: [1, 2, 3, 4, 5, 6, 7, 8, 9, 19, 30]).each do |nat|
        get "/quoc-gia/#{nat.slug}(/page/:page)", to: 'pages#nationality', id: nat.id
        get "/phim-bo/#{nat.slug}(/page/:page)", to: 'pages#nationality', id: nat.id, series: true
      end

      scope 'phim-le' do
        get '/(page/:page)', to: 'pages#year', all: true
        [*2012..2018].each do |year|
          get "#{year}(/page/:page)", to: 'pages#year', year: year
        end
        get "truoc-2012(/page/:page)", to: 'pages#year'
      end
      get '/phim-bo(/page/:page)' => 'pages#phimbo'
      get '*path', to: 'pages#index'
    end

  end
  get '*pages', to: 'home#index', :constraints => { :format => 'html' }
end
