set :stage, :production
set :rails_env, :production

server 'quayso.net', user: 'root', roles: %w{web app db}, primary: true